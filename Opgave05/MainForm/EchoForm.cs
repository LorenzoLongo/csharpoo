﻿using Globals.Interfaces;
using NAudio.Wave;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MainForm
{
    // SEE VIDEOS INSIDE OPGAVE05 FOLDER!
    public partial class EchoForm : Form
    {
        // Initializers
        private ILogic logic;
        private OpenFileDialog dialog;

        // variables
        private string path = @"Resources\";
        private int index = 0;

        // Constructor Form
        public EchoForm(ILogic logic)
        {
            InitializeComponent();
            this.logic = logic;
        }

        private void EchoForm_Load(object sender, EventArgs e)
        {

        }

        // Add audio source, update info into form
        private void addAudioBtn_Click(object sender, EventArgs e)
        {
            dialog = new OpenFileDialog();
            dialog.Filter = "Audio Files (*.WAV, *.MP3) | *.WAV; *.MP3";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                logic.SelectSound(dialog.FileName);

                // Update info into form (total time, playing time)
                fileNameTxtBox.Text = dialog.FileName;
                fileNameLbl.Text = Path.GetFileName(dialog.FileName);
                totalTimeLbl.Text = logic.AudioLength.ToString(@"hh\:mm\:ss");
                playTimeLbl.Text = logic.AudioPosition.ToString(@"hh\:mm\:ss");

                // Check if the extension is MP3, if yes convert to WAV
                index++;
                path = path + "test" + index + ".wav";
                if (Path.GetExtension(dialog.FileName).ToUpper() == ".MP3")
                {
                    ConvertMp3ToWav(dialog.FileName, path);
                }

                // Draw the WAV audiochart envelope
                DrawChart();
                EnableControls();
            }
        }


        // Drawa the envelope of the audio file
        public void DrawChart()
        {
            // Add Chartseries 
            waveChart.Series.Add("wave" + index);
            waveChart.Series["wave" + index].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.FastLine;
            waveChart.Series["wave" + index].ChartArea = "ChartArea1";

            // Convert to WaveChannel32
            NAudio.Wave.WaveChannel32 wave = new NAudio.Wave.WaveChannel32(new NAudio.Wave.WaveFileReader(path));

            // Initialize a buffer to set the wavevalues
            byte[] buffer = new byte[16384];
            int read = 0;

            // go through all the points inside the buffer and draw them onto the chart
            while (wave.Position < wave.Length)
            {
                read = wave.Read(buffer, 0, 16384);

                for (int i = 0; i < read / 4; i++)
                {
                    waveChart.Series["wave" + index].Points.Add(BitConverter.ToSingle(buffer, i * 4));
                }
            }
        }

        // Converts the MP3 to WAV
        private static void ConvertMp3ToWav(string filePath, string newFilePath)
        {
            // Read the MP3 file
            using (Mp3FileReader mp3 = new Mp3FileReader(filePath))
            {
                // Get the MP3 file stream and convert if to a wave file
                using (WaveStream pcm = WaveFormatConversionStream.CreatePcmStream(mp3))
                {
                    WaveFileWriter.CreateWaveFile(newFilePath, pcm);
                }
            }
        }

        // Enables the controls onto the form
        private void EnableControls()
        {
            playBtn.Enabled = pauseBtn.Enabled = volumeTrackBar.Enabled = echoTrackBar.Enabled = Enabled;
        }

        // Starts the audio file
        private void playBtn_Click(object sender, EventArgs e)
        {
            playBtn.Enabled = false;

            // Task to start the timer (total time played (real-time)) while playing the audio file
            Task job = Task.Run((Action)TimerSound);
            logic.OnUiUpdateNeeded += TimerSound;
            logic.Start();

            // Stop Task
            job.Wait();

        }

        // Counts the seconds played of the audio file (real-time)
        private void TimerSound()
        {
            BeginInvoke((MethodInvoker)delegate ()
            {
                // Play until the timers (labels) have equal values
                if (playTimeLbl.Text != totalTimeLbl.Text)
                {
                    playTimeLbl.Text = logic.AudioPosition.ToString(@"hh\:mm\:ss");
                }
            });
        }

        // Pause the audio file
        private void pauseBtn_Click(object sender, EventArgs e)
        {
            playBtn.Enabled = true;
            logic.Stop();
            logic.OnUiUpdateNeeded -= TimerSound;
        }

        // Set the volume of the audio file
        private void volumeTrackBar_Scroll(object sender, EventArgs e)
        {
            logic.Volume = (float)volumeTrackBar.Value / 10;
        }

        // Set the delay time for the echo effect
        private void echoTrackBar_Scroll(object sender, EventArgs e)
        {
            int dekay = echoTrackBar.Value;
            logic.Delay = echoTrackBar.Value;
            logic.DelayTime = TimeSpan.FromMilliseconds(echoTrackBar.Value);

            // Show delay in ms
            delayLbl.Text = logic.DelayTime.TotalMilliseconds.ToString() + "ms";
        }
    }
}
