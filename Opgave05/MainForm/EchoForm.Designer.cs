﻿namespace MainForm
{
    partial class EchoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EchoForm));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            this.addAudioBtn = new System.Windows.Forms.Button();
            this.fileNameTxtBox = new System.Windows.Forms.TextBox();
            this.playTextLbl = new System.Windows.Forms.Label();
            this.totalTextLbl = new System.Windows.Forms.Label();
            this.playTimeLbl = new System.Windows.Forms.Label();
            this.totalTimeLbl = new System.Windows.Forms.Label();
            this.playBtn = new System.Windows.Forms.Button();
            this.pauseBtn = new System.Windows.Forms.Button();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.echoTrackBar = new System.Windows.Forms.TrackBar();
            this.lowSoundPicBox = new System.Windows.Forms.PictureBox();
            this.highSoundPicBox = new System.Windows.Forms.PictureBox();
            this.echoLbl = new System.Windows.Forms.Label();
            this.nameLbl = new System.Windows.Forms.Label();
            this.fileNameLbl = new System.Windows.Forms.Label();
            this.delayLbl = new System.Windows.Forms.Label();
            this.waveChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.echoTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowSoundPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.highSoundPicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveChart)).BeginInit();
            this.SuspendLayout();
            // 
            // addAudioBtn
            // 
            this.addAudioBtn.Location = new System.Drawing.Point(464, 40);
            this.addAudioBtn.Name = "addAudioBtn";
            this.addAudioBtn.Size = new System.Drawing.Size(125, 23);
            this.addAudioBtn.TabIndex = 0;
            this.addAudioBtn.Text = "Search Audio File";
            this.addAudioBtn.UseVisualStyleBackColor = true;
            this.addAudioBtn.Click += new System.EventHandler(this.addAudioBtn_Click);
            // 
            // fileNameTxtBox
            // 
            this.fileNameTxtBox.Enabled = false;
            this.fileNameTxtBox.Location = new System.Drawing.Point(12, 40);
            this.fileNameTxtBox.Name = "fileNameTxtBox";
            this.fileNameTxtBox.Size = new System.Drawing.Size(446, 20);
            this.fileNameTxtBox.TabIndex = 1;
            // 
            // playTextLbl
            // 
            this.playTextLbl.AutoSize = true;
            this.playTextLbl.Location = new System.Drawing.Point(25, 108);
            this.playTextLbl.Name = "playTextLbl";
            this.playTextLbl.Size = new System.Drawing.Size(52, 13);
            this.playTextLbl.TabIndex = 2;
            this.playTextLbl.Text = "Play time:";
            // 
            // totalTextLbl
            // 
            this.totalTextLbl.AutoSize = true;
            this.totalTextLbl.Location = new System.Drawing.Point(25, 134);
            this.totalTextLbl.Name = "totalTextLbl";
            this.totalTextLbl.Size = new System.Drawing.Size(56, 13);
            this.totalTextLbl.TabIndex = 3;
            this.totalTextLbl.Text = "Total time:";
            // 
            // playTimeLbl
            // 
            this.playTimeLbl.AutoSize = true;
            this.playTimeLbl.Location = new System.Drawing.Point(118, 108);
            this.playTimeLbl.Name = "playTimeLbl";
            this.playTimeLbl.Size = new System.Drawing.Size(49, 13);
            this.playTimeLbl.TabIndex = 4;
            this.playTimeLbl.Text = "00:00:00";
            // 
            // totalTimeLbl
            // 
            this.totalTimeLbl.AutoSize = true;
            this.totalTimeLbl.Location = new System.Drawing.Point(118, 134);
            this.totalTimeLbl.Name = "totalTimeLbl";
            this.totalTimeLbl.Size = new System.Drawing.Size(49, 13);
            this.totalTimeLbl.TabIndex = 5;
            this.totalTimeLbl.Text = "00:00:00";
            // 
            // playBtn
            // 
            this.playBtn.Enabled = false;
            this.playBtn.Image = ((System.Drawing.Image)(resources.GetObject("playBtn.Image")));
            this.playBtn.Location = new System.Drawing.Point(219, 82);
            this.playBtn.Name = "playBtn";
            this.playBtn.Size = new System.Drawing.Size(61, 65);
            this.playBtn.TabIndex = 6;
            this.playBtn.UseVisualStyleBackColor = true;
            this.playBtn.Click += new System.EventHandler(this.playBtn_Click);
            // 
            // pauseBtn
            // 
            this.pauseBtn.Enabled = false;
            this.pauseBtn.Image = ((System.Drawing.Image)(resources.GetObject("pauseBtn.Image")));
            this.pauseBtn.Location = new System.Drawing.Point(298, 82);
            this.pauseBtn.Name = "pauseBtn";
            this.pauseBtn.Size = new System.Drawing.Size(61, 65);
            this.pauseBtn.TabIndex = 7;
            this.pauseBtn.UseVisualStyleBackColor = true;
            this.pauseBtn.Click += new System.EventHandler(this.pauseBtn_Click);
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.AutoSize = false;
            this.volumeTrackBar.Enabled = false;
            this.volumeTrackBar.Location = new System.Drawing.Point(219, 165);
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Size = new System.Drawing.Size(140, 45);
            this.volumeTrackBar.TabIndex = 1;
            this.volumeTrackBar.Value = 5;
            this.volumeTrackBar.Scroll += new System.EventHandler(this.volumeTrackBar_Scroll);
            // 
            // echoTrackBar
            // 
            this.echoTrackBar.Enabled = false;
            this.echoTrackBar.Location = new System.Drawing.Point(101, 216);
            this.echoTrackBar.Maximum = 1000;
            this.echoTrackBar.Name = "echoTrackBar";
            this.echoTrackBar.Size = new System.Drawing.Size(488, 45);
            this.echoTrackBar.TabIndex = 9;
            this.echoTrackBar.Value = 500;
            this.echoTrackBar.Scroll += new System.EventHandler(this.echoTrackBar_Scroll);
            // 
            // lowSoundPicBox
            // 
            this.lowSoundPicBox.Image = ((System.Drawing.Image)(resources.GetObject("lowSoundPicBox.Image")));
            this.lowSoundPicBox.Location = new System.Drawing.Point(184, 165);
            this.lowSoundPicBox.Name = "lowSoundPicBox";
            this.lowSoundPicBox.Size = new System.Drawing.Size(29, 25);
            this.lowSoundPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.lowSoundPicBox.TabIndex = 10;
            this.lowSoundPicBox.TabStop = false;
            // 
            // highSoundPicBox
            // 
            this.highSoundPicBox.Image = ((System.Drawing.Image)(resources.GetObject("highSoundPicBox.Image")));
            this.highSoundPicBox.Location = new System.Drawing.Point(365, 165);
            this.highSoundPicBox.Name = "highSoundPicBox";
            this.highSoundPicBox.Size = new System.Drawing.Size(29, 25);
            this.highSoundPicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.highSoundPicBox.TabIndex = 11;
            this.highSoundPicBox.TabStop = false;
            // 
            // echoLbl
            // 
            this.echoLbl.AutoSize = true;
            this.echoLbl.Location = new System.Drawing.Point(25, 227);
            this.echoLbl.Name = "echoLbl";
            this.echoLbl.Size = new System.Drawing.Size(70, 13);
            this.echoLbl.TabIndex = 12;
            this.echoLbl.Text = "Delay (echo):";
            // 
            // nameLbl
            // 
            this.nameLbl.AutoSize = true;
            this.nameLbl.Location = new System.Drawing.Point(25, 82);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(38, 13);
            this.nameLbl.TabIndex = 13;
            this.nameLbl.Text = "Name:";
            // 
            // fileNameLbl
            // 
            this.fileNameLbl.AutoSize = true;
            this.fileNameLbl.Location = new System.Drawing.Point(118, 82);
            this.fileNameLbl.Name = "fileNameLbl";
            this.fileNameLbl.Size = new System.Drawing.Size(16, 13);
            this.fileNameLbl.TabIndex = 14;
            this.fileNameLbl.Text = "...";
            // 
            // delayLbl
            // 
            this.delayLbl.AutoSize = true;
            this.delayLbl.Location = new System.Drawing.Point(25, 258);
            this.delayLbl.Name = "delayLbl";
            this.delayLbl.Size = new System.Drawing.Size(38, 13);
            this.delayLbl.TabIndex = 15;
            this.delayLbl.Text = "500ms";
            // 
            // waveChart
            // 
            chartArea1.Name = "ChartArea1";
            this.waveChart.ChartAreas.Add(chartArea1);
            this.waveChart.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.waveChart.Location = new System.Drawing.Point(0, 293);
            this.waveChart.Name = "waveChart";
            this.waveChart.Size = new System.Drawing.Size(610, 300);
            this.waveChart.TabIndex = 17;
            this.waveChart.Text = "chart1";
            // 
            // EchoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 593);
            this.Controls.Add(this.waveChart);
            this.Controls.Add(this.delayLbl);
            this.Controls.Add(this.fileNameLbl);
            this.Controls.Add(this.nameLbl);
            this.Controls.Add(this.echoLbl);
            this.Controls.Add(this.highSoundPicBox);
            this.Controls.Add(this.lowSoundPicBox);
            this.Controls.Add(this.echoTrackBar);
            this.Controls.Add(this.volumeTrackBar);
            this.Controls.Add(this.pauseBtn);
            this.Controls.Add(this.playBtn);
            this.Controls.Add(this.totalTimeLbl);
            this.Controls.Add(this.playTimeLbl);
            this.Controls.Add(this.totalTextLbl);
            this.Controls.Add(this.playTextLbl);
            this.Controls.Add(this.fileNameTxtBox);
            this.Controls.Add(this.addAudioBtn);
            this.Name = "EchoForm";
            this.Text = "Echo Editor";
            this.Load += new System.EventHandler(this.EchoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.echoTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lowSoundPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.highSoundPicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waveChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addAudioBtn;
        private System.Windows.Forms.TextBox fileNameTxtBox;
        private System.Windows.Forms.Label playTextLbl;
        private System.Windows.Forms.Label totalTextLbl;
        private System.Windows.Forms.Label playTimeLbl;
        private System.Windows.Forms.Label totalTimeLbl;
        private System.Windows.Forms.Button playBtn;
        private System.Windows.Forms.Button pauseBtn;
        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.TrackBar echoTrackBar;
        private System.Windows.Forms.PictureBox lowSoundPicBox;
        private System.Windows.Forms.PictureBox highSoundPicBox;
        private System.Windows.Forms.Label echoLbl;
        private System.Windows.Forms.Label nameLbl;
        private System.Windows.Forms.Label fileNameLbl;
        private System.Windows.Forms.Label delayLbl;
        private System.Windows.Forms.DataVisualization.Charting.Chart waveChart;
    }
}

