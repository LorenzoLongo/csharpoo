﻿using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace MainForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ILogic logic = new Logic();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new EchoForm(logic));
        }
    }
}
