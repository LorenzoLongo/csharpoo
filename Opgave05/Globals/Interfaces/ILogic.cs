﻿using AudioTools;
using System;

namespace Globals.Interfaces
{
    public interface ILogic : IDisposable
    {
        // Length of selected audio file
        TimeSpan AudioLength { get; }

        // Position where the audio file is playing
        TimeSpan AudioPosition { get; }

        // Delaytime to become echo sound
        TimeSpan DelayTime { get; set; }

        // Maximum possible delaytime
        TimeSpan MaxDelayTime { get; }

        // Maximum positive amplitude of an audiosignal in 3ms
        CircularBuffer<float> MaxQueue { get; }

        // Maximum negative amplitude of an audiosignal in 3ms
        CircularBuffer<float> MinQueue { get; }

        // Auto-implemented property to get and set the volume
        float Volume { get; set; }

        int Delay { get; set; }

        // Initializes the AudioReader and AudioPlayer classes
        void SelectSound(string filename);

        // Starts playing the audio file
        void Start();

        // Interrupts playing the audio file
        void Stop();

        // Tells the interface that the audio file is done playing
        event Action OnEndOfAudio;

        // Updates audio visualisation and file to actual value (to interface)
        event Action OnUiUpdateNeeded;
    }
}
