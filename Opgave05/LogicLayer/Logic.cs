﻿using AudioTools;
using Globals.Interfaces;
using System;

namespace LogicLayer
{
    public class Logic : ILogic
    {
        // Initializers of AudioPlayer, AudioReader and DelayLine classes
        private AudioPlayer player;
        private AudioReader reader;
        private DelayLine<int> delay = new DelayLine<int>(1000);

        // See ILogic interface for info
        public TimeSpan AudioLength { get { return reader.TimeLength; } }

        public TimeSpan AudioPosition { get { return reader.TimePosition; } }

        public TimeSpan DelayTime { get; set; }

        public TimeSpan MaxDelayTime { get; }

        public CircularBuffer<float> MaxQueue { get; }

        public CircularBuffer<float> MinQueue { get; }

        public float Volume
        {
            get { return reader.Volume; }
            set { reader.Volume = value; }
        }

        // Delay property for DelayLine class
        public int Delay
        {
            get { return Delay; }
            set { delay.Delay = value; }
        }

        // See ILogic interface for info
        public event Action OnEndOfAudio;
        public event Action OnUiUpdateNeeded;

        // Dispose method of AudioReader and AudioPlayer classes
        public void Dispose()
        {
            reader.Dispose();
            player.Dispose();
        }

        // Sets the AudioReader and AudioPlayer constructors
        public void SelectSound(string filename)
        {
            reader = new AudioReader(filename);

            // Tried the delay with this lines of code
            delay.Enqueue(reader.SampleRate);
            player = new AudioPlayer(delay.Dequeue());
        }

        // Starts to play the audio file 
        public void Start()
        {
            // Provides the samples of the audio file to play
            player.OnSampleFramesNeeded += Frames;
            player.Start();
        }

        // Stops to the play the audio file
        public void Stop()
        {
            player.Stop();
            player.OnSampleFramesNeeded -= Frames;
        }

        // Method to provide the frames for the sample
        private void Frames(int frames)
        {
            OnUiUpdateNeeded();

            for (int i = 0; i < frames; i++)
            {
                player.WriteSampleFrame(reader.ReadSampleFrame());
            }
        }
    }
}
