﻿using Globals.Classes;
using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface which contains the methods and property to get the CommitInfo for the local selected repository 
    public interface ILocalRepoManager
    {
        string CurrentRepoPath { get; }

        List<CommitInfo> GetCommits(AssignmentInfo assignment);

        void SelectLocalRepo(string path);
    }
}
