﻿using System;

namespace Globals.Classes
{
    // Class which contains info about a selected assignment inside a selecte repository
    public class AssignmentInfo
    {
        // Auto-implemented property which contains the enddate of an assignment
        public DateTime End { get; }

        // Auto-implemented property which contains the name of an assignment
        public string Name { get; }

        // Auto-implemented property which contains the startdate of an assignment
        public DateTime Start { get; }

        // Constructor of the AssignmentInfo class
        public AssignmentInfo(string name, DateTime start, DateTime end)
        {
            this.Name = name;
            this.Start = start;
            this.End = end;
        }
    }
}
