﻿using System;

namespace Globals.Classes
{
    // Class which contains info about each commit inside a selected assignment of a selected repository
    public class CommitInfo
    {
        // Auto-implemented property of the commit ID
        public string Id { get; }

        // Auto-implemented property which contains the DateTime of the commit
        public DateTime TimeStamp { get; }

        //  Constructor of the CommitInfo class
        public CommitInfo(string id, DateTime timestamp)
        {
            this.Id = id;
            this.TimeStamp = timestamp;
        }
    }
}
