﻿using DataLayer;
using Globals.Interfaces;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace CommitMonitorMain
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ILocalRepoManager access = new LocalRepoManager(@"JSON\Assignments.json");
            IRemoteDataManager remote = new RemoteDataManager(@"JSON\Assignments.json");
            IRemoteDataManager webRemote = new WebRemoteDataManager();
            ILogic logic = new Logic(access, webRemote);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(logic));
        }
    }
}
