﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicLayer;
using System.Drawing;
using Globals.Classes;
using System.Collections.Generic;
using System.Linq;
using Globals.Interfaces;
using System.Reflection;
using DataLayer;

namespace UnitTests
{
    [TestClass]
    public class _TestExtendedLogic
    {
        [TestMethod, Timeout(100)]
        public void CheckILogic()
        {
            const string interfaceName = "ILogic";
            Type x = Helper.GetTypeByName(interfaceName);
            Assert.IsNotNull(x, $"Interface \'{interfaceName}\' is not declared!");
            Helper.CheckProperty(x, propName: "AssignmentInfos",
                                    propTypes: new Type[] { typeof(List<AssignmentInfo>) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "CurrentRepoPath",
                                    propTypes: new Type[] { typeof(string) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "PersonalCommits",
                                    propTypes: new Type[] { typeof(IDrawable) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "AllCommits",
                                    propTypes: new Type[] { typeof(IDrawable) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckMethod(x, methodName: "SelectAssignment",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(AssignmentInfo) });
            Helper.CheckMethod(x, methodName: "SelectLocalRepo",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(string) });
        }

        [TestMethod, Timeout(100)]
        public void ImplementsInterface()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(Logic),
                                                 implementedInterface: typeof(ILogic));
        }

        [TestMethod, Timeout(100)]
        public void LinksToDataLayerOK()
        {
            Helper.CheckConstructors(typeof(Logic),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(ILocalRepoManager), typeof(IRemoteDataManager) });
            Type x = typeof(Logic);
            var fields = x.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            var OK = (fields.Where(f => f.FieldType == typeof(LocalRepoManager)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'LocalRepoManager\' (must use injected interface instead)!");
            OK = (fields.Where(f => f.FieldType == typeof(WebRemoteDataManager)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'RemoteDataManager\' (must use injected interface instead)!");
        }


        [TestMethod, ]
        public void CheckRemoteData()
        {
            var x = new Logic(new DummyLocalRepoManager(), new DummyWebRemoteDataManager());
            x.SelectAssignment(DummyRemoteDataManager.Assignments.First());
            Assert.IsNotNull(x.PersonalCommits, $"After selecting an assignment \'PersonalCommits\' returns null value.");
            var bitmap1 = new Bitmap(100, 100);
            x.PersonalCommits.Draw(bitmap1);
            x.SelectAssignment(DummyRemoteDataManager.Assignments.Last());
            var bitmap2 = new Bitmap(100, 100);
            x.PersonalCommits.Draw(bitmap2);
            Assert.IsFalse(Helper.IsSameBitmap(bitmap1, bitmap2, 0, 99), $"After selecting different assignments \'PersonalCommits\' does not generate different graphics.");
            x.SelectAssignment(DummyRemoteDataManager.Assignments.First());
            Assert.IsNotNull(x.AllCommits, $"After selecting an assignment \'PersonalCommits\' returns null value.");
            bitmap1 = new Bitmap(100, 100);
            x.AllCommits.Draw(bitmap1);
            x.SelectAssignment(DummyRemoteDataManager.Assignments.Last());
            bitmap2 = new Bitmap(100, 100);
            x.AllCommits.Draw(bitmap2);
            Assert.IsFalse(Helper.IsSameBitmap(bitmap1, bitmap2, 0, 99), $"After selecting different assignments \'AllCommits\' does not generate different graphics.");
        }
        

        private bool IsSameList(List<AssignmentInfo> l1, List<AssignmentInfo> l2)
        {
            if(l1.Count != l2.Count) return false;
            var l1s = l1.OrderBy(a => a.Name).ToList();
            var l2s = l2.OrderBy(a => a.Name).ToList();
            for(int i = 0; i < l1.Count; i++)
            {
                if(l1s[i].Name != l2s[i].Name) return false;
            }
            return true;
        }

        class DummyLocalRepoManager:ILocalRepoManager
        {
            public const string BadPath = "niets";
            public const string DefaultPath = @"C:\Windows";

            private string path = DefaultPath;

            public string CurrentRepoPath => path == BadPath ? string.Empty : path;

            public List<CommitInfo> GetCommits(AssignmentInfo assignment)
            {
                if(path == BadPath) return new List<CommitInfo>();
                if(assignment.Name == "A") return new List<CommitInfo>();
                return new List<CommitInfo>()
                                            {
                                                new CommitInfo("A", DateTime.Now.AddDays(-5).AddMinutes(5)),
                                                new CommitInfo("B", DateTime.Now.AddDays(-5).AddMinutes(6)),
                                            };
            }

            public void SelectLocalRepo(string path)
            {
                this.path = path;
            }
        }

        class DummyWebRemoteDataManager:IRemoteDataManager
        {
            
            public static List<AssignmentInfo> Assignments = new List<AssignmentInfo>
            {
                new AssignmentInfo("A", DateTime.Now.AddDays(-9), DateTime.Now.AddDays(-7)),
                new AssignmentInfo("B", DateTime.Now.AddDays(-5), DateTime.Now.AddDays(-3))
            };

            public List<CommitInfo> GetAllCommits(AssignmentInfo assignment)
            {
                if(assignment.Name == "A") return new List<CommitInfo>();
                return new List<CommitInfo>()
                {
                   new CommitInfo("A", DateTime.Now.AddDays(-5).AddMinutes(5)),
                   new CommitInfo("B", DateTime.Now.AddDays(-5).AddMinutes(6)),
                   new CommitInfo("C", DateTime.Now.AddDays(-5).AddMinutes(7)),
                   new CommitInfo("D", DateTime.Now.AddDays(-5).AddMinutes(8)),
                };
            }

            public List<AssignmentInfo> GetAssignments()
            {
                return Assignments;
            }

            public int GetNrOfStudents()
            {
                return 2;
            }
        }

    }
}
