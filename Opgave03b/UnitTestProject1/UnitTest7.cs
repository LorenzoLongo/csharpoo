﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Globals.Interfaces;
using DataLayer;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class _TestWebRemoteDatamanager
    {
        [TestMethod, Timeout(100)]
        public void ImplementsInterface()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(WebRemoteDataManager),
                                                 implementedInterface: typeof(IRemoteDataManager));
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            Helper.CheckConstructors(typeof(WebRemoteDataManager),
                                     hasDefault: true,
                                     hasSpecific: false,
                                     specificParameters: new Type[] { });
        }


        [TestMethod, Timeout(10000)]
        public void TestNrOfStudents()
        {
            IRemoteDataManager manager = new WebRemoteDataManager();
            int[] results = new int[5];
            for(int i = 0; i < results.Length; i++)
            {              
                results[i] = manager.GetNrOfStudents();                
            }
                        
            Assert.IsFalse(results.Contains(0), "GetNrOfStudents does not always succeed.");
        }

        [TestMethod, Timeout(10000)]
        public void TestAssignments()
        {
            IRemoteDataManager manager = new WebRemoteDataManager();
            var assignments = manager.GetAssignments();
            Assert.IsFalse((assignments.Count == 0), "GetAssignments returns no results.");
            Assert.IsTrue(assignments[2].Name.Contains("3A"), "GetAssignments returns invalid data." );
            Assert.IsTrue(assignments[2].Start== new DateTime(2017,10,24), "GetAssignments returns invalid data.");
        }

        [TestMethod, Timeout(10000)]
        public void TestCommits()
        {
            IRemoteDataManager manager = new WebRemoteDataManager();
            var assignments = manager.GetAssignments();
            Assert.IsFalse((assignments.Count == 0), "GetAssignments returns no results.");
            Assert.IsTrue(assignments[2].Name.Contains("3A"), "GetAssignments returns invalid data;");
            Assert.IsTrue(assignments[2].Start == new DateTime(2017, 10, 24), "GetAssignments returns invalid data;");
            var commits = manager.GetAllCommits(assignments[0]);
            Assert.IsTrue(commits.Count == 723, $"GetAllCommits return wrong number of commits: {commits.Count}");

        }
    }
}
