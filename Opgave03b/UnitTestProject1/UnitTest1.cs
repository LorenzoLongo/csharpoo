﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Globals.Classes;
using System.IO;
using System.Reflection;
using System.Drawing;

namespace UnitTests
{
    static class Helper
    {
        static private bool dummy = LoadAllAssemblies();
        private static bool LoadAllAssemblies()
        {
       
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var di = new DirectoryInfo(path);
            var debug = di.GetFiles("*.*");
            foreach(var file in di.GetFiles("*.dll"))
            {
                try
                {
                    var nextAssembly = Assembly.LoadFile(file.FullName);
                }
                catch(BadImageFormatException)
                {
                    // Not a .net assembly  - ignore
                }
            }
            return true;        
        }
   
        public static Type GetTypeByName(string typeName)
        {
            var x = AppDomain.CurrentDomain.GetAssemblies();
            var foundClass = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                              from type in assembly.GetTypes()
                              where type.Name == typeName
                              select type).FirstOrDefault();
            return (Type)foundClass;
        }
        public static void CheckProperty(Type t, string propName, Type[] propTypes, bool hasPublicGetter, bool hasPublicSetter, bool hasPrivateGetter, bool hasPrivateSetter)
        {
            var props = t.GetProperties();
            var prop = props.Where(p => p.Name == propName).FirstOrDefault();
            Assert.IsNotNull(prop, $"Type {t.GetType().Name} has no public \'{propName}\' property");
            Assert.IsTrue(Array.Exists(propTypes, p => p.Name == prop.PropertyType.Name),
                              $"{typeof(object).Name}: property \'{propName}\' is a {prop.PropertyType.Name}");
            if(hasPrivateGetter || hasPublicGetter)
            {
                Assert.IsNotNull(prop.GetMethod, $"{t.Name}: property {propName} has no Getter ");
                Assert.IsTrue((prop.GetMethod.IsPublic == hasPublicGetter), $"{t.Name}: property {propName} has {(prop.GetMethod.IsPublic ? "a" : "no")} public Getter ");
                Assert.IsTrue((prop.GetMethod.IsPrivate == hasPrivateGetter), $"{t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Getter ");
            }
            else
            {
                Assert.IsNull(prop.GetMethod, $"{t.Name}: property {propName} has a Getter (not allowed)");
            }
            if(hasPrivateSetter || hasPublicSetter)
            {
                Assert.IsNotNull(prop.SetMethod, $"{t.Name}: property {propName} has no Setter ");
                Assert.IsTrue((prop.SetMethod.IsPublic == hasPublicSetter), $"GlobalTests - {t.Name}: property {propName} has {(prop.SetMethod.IsPublic ? "a" : "no")} public Setter ");
                Assert.IsTrue((prop.SetMethod.IsPrivate == hasPrivateSetter), $"GlobalTests - {t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Setter ");
            }
            else
            {
                Assert.IsNull(prop.SetMethod, $"{t.Name}: property {propName} has a Setter (not allowed)");
            }
        }
        public static void CheckMethod(Type t, string methodName, Type[] returnTypes, Type[] parameterTypes)
        {
            var methods = t.GetMethods();
            // check if method exists with right signature
            var method = methods.Where(m =>
            {
                if(m.Name != methodName) return false;
                var parameters = m.GetParameters();
                if((parameterTypes == null || parameterTypes.Length == 0)) return parameters.Length == 0;
                if(parameters.Length != parameterTypes.Length) return false;
                for(int i = 0; i < parameterTypes.Length; i++)
                {
                    // if (parameters[i].ParameterType != parameterTypes[i])
                    if(!parameters[i].ParameterType.IsAssignableFrom(parameterTypes[i]))
                        return false;
                }
                return true;
            }).FirstOrDefault();
            Assert.IsNotNull(method, $"Type {t.FullName} has no public \'{methodName}\' method with the right signature");

            // check returnType
            Assert.IsTrue(Array.Exists(returnTypes, r => r.Name == method.ReturnType.Name),
                              $"Type {typeof(object).Name}: method \'{methodName}\' returns a \'{method.ReturnType.Name}\'");
        }
        public static void CheckClassImplementsInterface(Type implementingClass, Type implementedInterface)
        {
            Assert.IsTrue(implementedInterface.IsAssignableFrom(implementingClass),$"\'{implementingClass.Name}\' does not implement interface \'{implementedInterface}\'.");
        }
        public static void CheckConstructors(Type x, bool hasDefault, bool hasSpecific, Type[] specificParameters)
        {
            var constructor = x.GetConstructor(new Type[] { });
            if(hasDefault) Assert.IsNotNull(constructor, $"\'{x.Name}\' has no default constructor (which is required)!");
            if(!hasDefault) Assert.IsNull(constructor, $"\'{x.Name}\' has no a default constructor (not allowed)!");
            if(hasSpecific)
            {
                constructor = x.GetConstructor(specificParameters);
                string parameters = specificParameters.Select(p => $"\'{p.Name}\'").Aggregate((temp, element) => temp + "," + element);
                Assert.IsNotNull(constructor,
                    $"\'{x.Name}\' does not contain a constructor with following parameter(s) of type(s): {parameters}.");
            }            
        }
        public static void CheckField(Type t, string fieldName, Type[] fieldTypes, bool isStatic, bool isProtected, bool isConstant, bool isReadOnly)
        {
            var field = t.GetField(fieldName, BindingFlags.DeclaredOnly | BindingFlags.NonPublic|BindingFlags.Instance|BindingFlags.Static);
            Assert.IsNotNull(field, $"Type {t.Name} has no \'{fieldName}\' field");
            Assert.IsTrue(fieldTypes.Contains(field.FieldType),$"Field \'{fieldName}\' should be of type \'{fieldTypes[0]}\' but is of type \'{field.FieldType}\'.");
            Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Public), $"Field \'{fieldName}\' is public but should not be.");
            if(isConstant) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Static)&& field.Attributes.HasFlag(FieldAttributes.Literal), $"Field \'{fieldName}\' should be a constant but is not.");
            if(!isConstant)
            {
                Assert.IsTrue(!field.Attributes.HasFlag(FieldAttributes.Static) || !field.Attributes.HasFlag(FieldAttributes.Literal), $"Field \'{fieldName}\' is a constant but should not be.");
                if(isStatic) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Static), $"Field \'{fieldName}\' should be static but is not.");
                if(!isStatic) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Static), $"Field \'{fieldName}\' is static but should not be.");
            }
            if(isProtected) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Family), $"Field \'{fieldName}\' should be protected (not private) but is not.");
            if(!isProtected) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Family), $"Field \'{fieldName}\' is protected, should be private.");
            if(isReadOnly) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.InitOnly), $"Field \'{fieldName}\' should be readonly but is not.");
            if(!isReadOnly) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.InitOnly), $"Field \'{fieldName}\' is readonly but should not be.");
        }
        public static bool IsSameBitmap(Bitmap b1, Bitmap b2, int startColumn, int endColumn)
        {
            for(int i = startColumn; i <= endColumn; i++)
            {
                for(int j = 0; j < b1.Height; j++)
                {
                    if(b1.GetPixel(i, j) != b2.GetPixel(i, j)) return false;
                }
            }
            return true;
        }


    }



    [TestClass]
    public class TestGlobalClasses
    {

        [TestMethod, Timeout(100)]
        public void CommitInfoProperties()
        {
            var x = typeof(CommitInfo);
            Helper.CheckProperty(x, propName: "Id",
                                    propTypes: new Type[] { typeof(string) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "TimeStamp",
                                    propTypes: new Type[] { typeof(DateTime) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
        }

        [TestMethod, Timeout(100)]
        public void CommitInfoConstructors()
        {
            Helper.CheckConstructors(typeof(CommitInfo),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(string), typeof(DateTime) });

        }
        
        [TestMethod, Timeout(100)]
        public void AssignmentInfoProperties()
        {
            var x = typeof(AssignmentInfo);
            Helper.CheckProperty(x, propName: "Name",
                                    propTypes: new Type[] { typeof(string) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "Start",
                                    propTypes: new Type[] { typeof(DateTime) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "End",
                                    propTypes: new Type[] { typeof(DateTime) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
        }

        [TestMethod, Timeout(100)]
        public void AssignmentInfoConstructors()
        {
            Helper.CheckConstructors(typeof(AssignmentInfo),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(string), typeof(DateTime), typeof(DateTime) });            
        }
    }

}
