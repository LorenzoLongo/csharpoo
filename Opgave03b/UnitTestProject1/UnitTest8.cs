﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Globals.Interfaces;
using LogicLayer;
using Globals.Classes;
using System.Collections.Generic;
using System.Drawing;

namespace UnitTests
{
    [TestClass]
    public class _TestAllCommitStatistics
    {
        [TestMethod, Timeout(100)]
        public void InheritsFromCommitStatistics()
        {
            Assert.IsTrue(typeof(AllCommitStatistics).IsSubclassOf(typeof(CommitStatistics)), $"\'AllCommitStatistics\' is not a subclass of \'CommitStatistics\'");
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            Helper.CheckConstructors(typeof(AllCommitStatistics),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(AssignmentInfo), typeof(List<CommitInfo>), typeof(int) });
        }


        [TestMethod, Timeout(1000)]
        public void CanInstantiate()
        {
            var x = new AllCommitStatistics(
                                            new AssignmentInfo("test", DateTime.Now.AddDays(-1), DateTime.Now),
                                            new List<CommitInfo>(),
                                            1
                                        );
            Assert.IsNotNull(x, $"Could not instantiate \' CommitStatistics\'");
        }

        [TestMethod, Timeout(1000)]
        public void InitialisesValuesOK()
        {
            var x = new AllCommitStatisticsProxy(
                                            new AssignmentInfo("test", DateTime.Now.AddDays(-5), DateTime.Now),
                                            new List<CommitInfo>
                                            {
                                               new CommitInfo("A", DateTime.Now.AddDays(-5).AddMinutes(5)),
                                               new CommitInfo("B", DateTime.Now.AddDays(-5).AddMinutes(6)),
                                               new CommitInfo("C", DateTime.Now.AddDays(-5).AddMinutes(7)),
                                               new CommitInfo("D", DateTime.Now.AddDays(-5).AddMinutes(8))
                                            }, 
                                            2); 
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            Assert.IsNotNull(x.CommitValues, $"\'Values\' property is null after calling \'CommitStatistics\' constructor.");
            Assert.IsTrue(x.CommitValues.Length == 5, $"\'CommitStatistics\' constructor test initializes \'Values\' property with length {x.CommitValues.Length}, should be 5.");
        }

       
        class AllCommitStatisticsProxy:AllCommitStatistics
        {
            public int[] CommitValues => Values;

            public AllCommitStatisticsProxy(AssignmentInfo assignment, List<CommitInfo> commits, int count)
                : base(assignment, commits, count)
            { }
        }


        [TestMethod, Timeout(1000)]
        public void DrawsCommits()
        {
            var x = new AllCommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() { },2);
            var y = new AllCommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() {
                                                new CommitInfo("1", DateTime.Now.AddDays(-1).AddMinutes(1)),
                                                new CommitInfo("2", DateTime.Now.AddDays(-1).AddMinutes(2)),
                                                new CommitInfo("3", DateTime.Now.AddDays(-1).AddMinutes(3)),
                                                new CommitInfo("4", DateTime.Now.AddDays(-1).AddMinutes(4)),
                                                new CommitInfo("5", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("6", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                                new CommitInfo("7", DateTime.Now.AddDays(-1).AddMinutes(7)),
                                                new CommitInfo("8", DateTime.Now.AddDays(-1).AddMinutes(8)),
                                                new CommitInfo("9", DateTime.Now.AddDays(-1).AddMinutes(9)),
                                                new CommitInfo("10", DateTime.Now.AddDays(-1).AddMinutes(10))
                                            },2);
            Assert.IsNotNull(x, $"Could not instantiate \'AllmmitStatistics\'");
            Assert.IsNotNull(y, $"Could not instantiate \'AllCommitStatistics\' with filled commit-list.");
            const int size = 100;
            using(var bitmap = new Bitmap(size, size))
            {
                x.Draw(bitmap);
                using(var originalBitmap = new Bitmap(bitmap))
                {
                    y.Draw(bitmap);
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width - 1), $"AllCommitStatistics \'Draw\' method does not draw commit-info");
                }
            }
        }

        [TestMethod,]
        public void DrawsCommitsPlausibleX()
        {
            var x = new AllCommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() { },2);
            var y = new AllCommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() {
                                                new CommitInfo("1", DateTime.Now.AddDays(-1).AddMinutes(1)),
                                                new CommitInfo("2", DateTime.Now.AddDays(-1).AddMinutes(2)),
                                                new CommitInfo("3", DateTime.Now.AddDays(-1).AddMinutes(3)),
                                                new CommitInfo("4", DateTime.Now.AddDays(-1).AddMinutes(4)),
                                                new CommitInfo("5", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("6", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                                new CommitInfo("7", DateTime.Now.AddDays(-1).AddMinutes(7)),
                                                new CommitInfo("8", DateTime.Now.AddDays(-1).AddMinutes(8)),
                                                new CommitInfo("9", DateTime.Now.AddDays(-1).AddMinutes(9)),
                                                new CommitInfo("10", DateTime.Now.AddDays(-1).AddMinutes(10))
                                            },2);
            Assert.IsNotNull(x, $"Could not instantiate \'AllCommitStatistics\'");
            Assert.IsNotNull(y, $"Could not instantiate \'AllCommitStatistics\' with filled commit-list.");
            const int size = 100;
            using(var bitmap = new Bitmap(size, size))
            {
                x.Draw(bitmap);
                using(var originalBitmap = new Bitmap(bitmap))
                {
                    y.Draw(bitmap);
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width - 1), $"AllCommitStatistics \'Draw\' method does not draw commit-info");
                    Assert.IsTrue(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width / 3), $"AllCommitStatistics \'Draw\' method does not draw commit-info correctly");
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, (2 * bitmap.Width) / 3, bitmap.Width - 1), $"AllCommitStatistics \'Draw\' method does not draw commit-info correctly");
                }
            }
        }
               
    }
}
