﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataLayer;
using Globals.Interfaces;
using System.IO;
using System.Linq;

namespace UnitTests
{
    [TestClass]
    public class TestRemoteDataManager
    {
        [TestMethod, Timeout(100)]
        public void ImplementsInterface()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(RemoteDataManager),
                                                 implementedInterface: typeof(IRemoteDataManager));
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            Helper.CheckConstructors(typeof(RemoteDataManager),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(string)});
        }


        [TestMethod, Timeout(1000)]
        public void TestGetAssignmentsFromInvalidPath()
        {
            const string jsonPath = "niets.json";
            var manager = new RemoteDataManager(jsonPath);
            var result = manager.GetAssignments();
            Assert.IsTrue(result.Count == 0, $"\'GetAssignments\' does not return an empty List for an invalid path for the json-file.");
        }

        [TestMethod, Timeout(1000)]
        public void TestGetAssignmentsFromValidPath()
        {
            string testJson = @"[{""Name"":""Test1"",""Start"":""2017-09-27T00:00:00"",""End"":""2017-09-28T00:00:00""},{ ""Name"":""Test2"",""Start"":""2017-10-04T00:00:00"",""End"":""2017-10-05T00:00:00""}]";
            const string jsonPath = "test.json";
            File.WriteAllText(jsonPath, testJson);
            var manager = new RemoteDataManager(jsonPath);
            var result = manager.GetAssignments();
            try
            {
                Assert.IsTrue(result.Count == 2, $"\'GetAssignments\' does not return the right number of assignments: 2 expected, returned {result.Count}.");
                Assert.IsTrue(result.First().Name == "Test1", $"\'GetAssignments\' does not return the right assignments: \'test1\' expected, returned {result.First().Name}.");
            }
            finally
            {
                File.Delete(jsonPath);
            }
        }

    }
}
