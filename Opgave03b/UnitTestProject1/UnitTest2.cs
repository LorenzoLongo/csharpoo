﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Globals.Classes;
using Globals.Interfaces;
using System.Collections.Generic;


namespace UnitTests
{
    [TestClass]
    public class TestGlobalInterfaces
    {
        [TestMethod, Timeout(100) ]
        public void CheckIDrawable()
        {
            const string interfaceName = "IDrawable";
            Type x = Helper.GetTypeByName(interfaceName);
            Assert.IsNotNull(x, $"Interface \'{interfaceName}\' is not declared!");
            Helper.CheckMethod(x, methodName: "Draw",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(System.Drawing.Bitmap) });
        }

        [TestMethod, Timeout(100)]
        public void CheckILocalRepoManager()
        {
            const string interfaceName = "ILocalRepoManager";
            Type x = Helper.GetTypeByName(interfaceName);
            Assert.IsNotNull(x, $"Interface \'{interfaceName}\' is not declared!");
            Helper.CheckProperty(x, propName: "CurrentRepoPath",
                                    propTypes: new Type[] { typeof(string) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckMethod(x, methodName: "SelectLocalRepo",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(string) });
            Helper.CheckMethod(x, methodName: "GetCommits",
                                  returnTypes: new Type[] { typeof(List<CommitInfo>) },
                                  parameterTypes: new Type[] { typeof(AssignmentInfo) });
        }

        [TestMethod, Timeout(100)]
        public void CheckIRemoteDataManager()
        {
            const string interfaceName = "IRemoteDataManager";
            Type x = Helper.GetTypeByName(interfaceName);
            Assert.IsNotNull(x, $"Interface \'{interfaceName}\' is not declared!");
            
            Helper.CheckMethod(x, methodName: "GetAssignments",
                                  returnTypes: new Type[] { typeof(List<AssignmentInfo>) },
                                  parameterTypes: new Type[] { });
            Helper.CheckMethod(x, methodName: "GetNrOfStudents",
                                  returnTypes: new Type[] { typeof(int) },
                                  parameterTypes: new Type[] { });
            Helper.CheckMethod(x, methodName: "GetAllCommits",
                                  returnTypes: new Type[] { typeof(List<CommitInfo>) },
                                  parameterTypes: new Type[] { typeof(AssignmentInfo)});
        }

        [TestMethod, Timeout(100)]
        public void CheckILogic()
        {
            const string interfaceName = "ILogic";
            Type x = Helper.GetTypeByName(interfaceName);
            Assert.IsNotNull(x, $"Interface \'{interfaceName}\' is not declared!");
            Helper.CheckProperty(x, propName: "AssignmentInfos",
                                    propTypes: new Type[] { typeof(List<AssignmentInfo>) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "CurrentRepoPath",
                                    propTypes: new Type[] { typeof(string) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckProperty(x, propName: "PersonalCommits",
                                    propTypes: new Type[] { typeof(IDrawable) },
                                    hasPublicGetter: true,
                                    hasPublicSetter: false,
                                    hasPrivateGetter: false,
                                    hasPrivateSetter: false);
            Helper.CheckMethod(x, methodName: "SelectAssignment",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(AssignmentInfo    ) });
            Helper.CheckMethod(x, methodName: "SelectLocalRepo",
                                  returnTypes: new Type[] { typeof(void) },
                                  parameterTypes: new Type[] { typeof(string) });
        }


    }

}
