﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Globals.Interfaces;
using LogicLayer;
using System.Reflection;
using System.Linq;
using DataLayer;
using Globals.Classes;
using System.Collections.Generic;
using System.IO;
using System.Drawing;

namespace UnitTests
{
    [TestClass]
    public class TestlogicImplementation
    {
        [TestMethod, Timeout(100)]
        public void ImplementsInterface()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(Logic),
                                                 implementedInterface: typeof(ILogic));
        }
        
        [TestMethod, Timeout(100)]
        public void LinksToDataLayerOK()
        {
            Helper.CheckConstructors(typeof(Logic),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(ILocalRepoManager), typeof(IRemoteDataManager) });
            Type x = typeof(Logic);
            var fields = x.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            var OK = (fields.Where(f => f.FieldType == typeof(LocalRepoManager)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'LocalRepoManager\' (must use injected interface instead)!");
            OK = (fields.Where(f => f.FieldType == typeof(RemoteDataManager)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'RemoteDataManager\' (must use injected interface instead)!");
        }

        [TestMethod, Timeout(1000)]
        public void CheckLocalRepo()
        {
            var x = new Logic(new DummyLocalRepoManager(), new DummyRemoteDataManager());
            Assert.IsTrue(DummyLocalRepoManager.DefaultPath.ToLower().Contains(x.CurrentRepoPath.ToLower()), $"\'CurrentRepoPath\' does not return value exposed by LocalRepoManager).");
            x.SelectLocalRepo(DummyLocalRepoManager.BadPath);
            Assert.IsTrue(x.CurrentRepoPath == string.Empty, $"\'SelectLocalrepo\' method does not pass the path correctly to the LocalrepoManager.");
            var path = @"C:\niets\testrepo";
            x.SelectLocalRepo(path);
            Assert.IsFalse(x.CurrentRepoPath.ToLower() == path.ToLower(), $"\'CurrentRepoPath\' returns full path of repository (should be only lowest level).");
            Assert.IsTrue(path.ToLower().Contains(x.CurrentRepoPath.ToLower()), $"\'CurrentRepoPath\' returns wrong repository name).");

        }

        [TestMethod, Timeout(1000)]
        public void CheckRemoteData()
        {
            var x = new Logic(new DummyLocalRepoManager(), new DummyRemoteDataManager());
            Assert.IsTrue(IsSameList(x.AssignmentInfos, DummyRemoteDataManager.Assignments), $"\'AssignmentInfos\' does not return values exposed by RemoteDataManager).");
            x.SelectAssignment(DummyRemoteDataManager.Assignments.First());
            Assert.IsNotNull(x.PersonalCommits, $"After selecting an assignment \'PersonalCommits\' returns null value.");
            var bitmap1 = new Bitmap(100, 100);
            x.PersonalCommits.Draw(bitmap1);
            x.SelectAssignment(DummyRemoteDataManager.Assignments.Last());
            var bitmap2 = new Bitmap(100, 100);
            x.PersonalCommits.Draw(bitmap2);
            Assert.IsFalse(Helper.IsSameBitmap(bitmap1, bitmap2,0,99), $"After selecting different assignments \'PersonalCommits\' does not generate different graphics.");
        }

        private bool IsSameList(List<AssignmentInfo> l1, List<AssignmentInfo> l2)
        {
            if(l1.Count != l2.Count) return false;
            var l1s = l1.OrderBy(a => a.Name).ToList();
            var l2s = l2.OrderBy(a => a.Name).ToList();
            for(int i = 0; i < l1.Count; i++)
            {
                if(l1s[i].Name != l2s[i].Name) return false;
            }
            return true;
        }

    }


    class DummyLocalRepoManager:ILocalRepoManager
    {
        public const string BadPath = "niets";
        public const string DefaultPath = @"C:\Windows";

        private string path = DefaultPath;

        public string CurrentRepoPath => path== BadPath ? string.Empty: path ;

        public List<CommitInfo> GetCommits(AssignmentInfo assignment)
        {
            if(path == BadPath) return new List<CommitInfo>();
            if (assignment.Name == "B") return new List<CommitInfo>();
            return new List<CommitInfo>()
                                            {
                                                new CommitInfo("A", DateTime.Now.AddDays(-9).AddMinutes(5)),
                                                new CommitInfo("B", DateTime.Now.AddDays(-9).AddMinutes(6)),
                                            };
        }

        public void SelectLocalRepo(string path)
        {
            this.path = path;
        }
    }

    class DummyRemoteDataManager:IRemoteDataManager
    {
        public static List<AssignmentInfo> Assignments = new List<AssignmentInfo>
            {
                new AssignmentInfo("A", DateTime.Now.AddDays(-10), DateTime.Now.AddDays(-7)),
                new AssignmentInfo("B", DateTime.Now.AddDays(-5), DateTime.Now.AddDays(-3))
            };

        public List<CommitInfo> GetAllCommits(AssignmentInfo assignment)
        {
            return new List<CommitInfo>();
        }

        public List<AssignmentInfo> GetAssignments()
        {
            return Assignments;
        }

        public int GetNrOfStudents()
        {
            return 2;
        }
    }



}
