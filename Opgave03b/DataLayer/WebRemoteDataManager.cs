﻿using Globals.Classes;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DataLayer
{
    // Class which returns the assignments, number of students and number of commits on GITIKDOEICT
    public class WebRemoteDataManager : IRemoteDataManager
    {
        // Initializing an object of the HttpClient class
        HttpClient client = new HttpClient();

        // Lists which contain the assignments and all of the commits
        private List<AssignmentInfo> assignments = new List<AssignmentInfo>();
        private List<CommitInfo> commits = new List<CommitInfo>();

        // The total number of students
        private int nrOfStudents;

        // Constructor of the WebRemoteDataManager class
        public WebRemoteDataManager()
        {
            // Sets the base uri, sets media type to JSON
            client.BaseAddress = new Uri("http://gitcommits.ikdoeict.net/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // Method which requests all of the commits of an assignment and returns the values inside a list
        public List<CommitInfo> GetAllCommits(AssignmentInfo assignment)
        {
            // Setting the uri with the necessary uri parameters (date formats)
            var response = client.GetAsync($"api/Commits/1718CSharp?start={assignment.Start:yyyy-MM-dd HH:mm:ss}&end={assignment.End:yyyy-MM-dd HH:mm:ss}").Result;

            // Checks if the uri gets a response, if 'yes' set CommitInfo list
            if (response.IsSuccessStatusCode)
            {
                commits = response.Content.ReadAsAsync<List<CommitInfo>>().Result;
            }
            return commits;
        }

        // Method which requests the name of the assignments and returns a list of all the assignment values
        public List<AssignmentInfo> GetAssignments()
        {
            // Setting the uri
            var response = client.GetAsync("api/assignments/1718CSharp").Result;

            // Checks if the uri gets a response, if 'yes' set AssignmentInfo list
            if (response.IsSuccessStatusCode)
            {

                assignments = response.Content.ReadAsAsync<List<AssignmentInfo>>().Result;
            }
            return assignments;
        }

        // Method which requests the total number of students who 'commit' inside the repository
        public int GetNrOfStudents()
        {
            // Setting the uri
            var response = client.GetAsync("api/count/1718CSharp").Result;

            // Checks if the uri gets a response, if 'yes' set integer nrOfStudents
            if (response.IsSuccessStatusCode)
            {
                nrOfStudents = response.Content.ReadAsAsync<int>().Result;
            }
            return nrOfStudents;
        }
    }
}
