﻿using Globals.Classes;
using Globals.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DataLayer
{
    // Class which contains all of the commitdata of all students
    public class RemoteDataManager : IRemoteDataManager
    {
        // Field of selected path
        private string path;

        // Constructor of RemoteDataManager class
        public RemoteDataManager(string jsonPath)
        {
            this.path = jsonPath;
        }


        // Method which gets all CommitInfo from all students (Not implemented)
        public List<CommitInfo> GetAllCommits(AssignmentInfo assignment)
        {
            throw new NotImplementedException();
        }

        // Method which gets a list of assignments and their information (from a selected path)
        public List<AssignmentInfo> GetAssignments()
        {
            // If JSON file is valid, deserialize the file and add to the AssignmentInfo list. Else return empty list
            if (File.Exists(path))
            {
                return JsonConvert.DeserializeObject<List<AssignmentInfo>>(File.ReadAllText(path));
            }
            else
            {
                return new List<AssignmentInfo>();
            }
        }

        // Method to get the total number of students (Not implemented)
        public int GetNrOfStudents()
        {
            throw new NotImplementedException();
        }
    }
}
