﻿using Globals.Classes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LogicLayer
{
    public class AllCommitStatistics : CommitStatistics
    {
        // Fields to set constructor parameter values
        private AssignmentInfo assignment;
        private int nrOfStudents;
        private List<CommitInfo> commits = new List<CommitInfo>();

        // Graphics class field
        private Graphics g;

        // Date field
        private DateTime date;

        // Black pen field
        private Pen blackPen = new Pen(Color.Black, 3);

        // Constant fields
        private int heightDivideByLimit;
        private int widthDivideByValues;
        private int graphPerCommit;

        // Array of Rectangles
        private Rectangle[] arrayGraphPerDay;

        // Array which contains the number of commits per day
        private double[] AverageValues;

        // Dictionary which contains the date and the number of commits that are done on that date
        private SortedDictionary<DateTime, int> Sorted = new SortedDictionary<DateTime, int>();

        // Constructor of the AllCommitsStatistics class
        public AllCommitStatistics(AssignmentInfo assignment, List<CommitInfo> commits, int nrOfStudents) : base(assignment, commits)
        {
            this.assignment = assignment;
            this.commits = commits;
            this.nrOfStudents = nrOfStudents;

            // Set the values of the integer array, divides the array by the number of students
            SetValues();
            SetAverageValues();
        }

        // Method to set the integer array Values
        private void SetValues()
        {
            // Initialize Sorted list with dates and 0 integer value
            for (date = assignment.Start.Date; date < assignment.End.Date; date = date.AddDays(1))
            {
                Sorted.Add(date.Date, 0);
            }

            // Check on which day a commit of the CommitInfo List is done (increment integer value by 1)
            date = assignment.Start.Date;
            do
            {
                foreach (CommitInfo dates in commits)
                {
                    if (dates.TimeStamp.Date == date.Date)
                    {
                        Sorted[date.Date] += 1;
                    }
                }
                date = date.AddDays(1).Date;
            } while (date.Date < assignment.End.Date);

            // Initialize Values array and fill with the Sorted List integer values
            Values = new int[Sorted.Count];
            Values = Sorted.Values.ToArray();
        }

        // Method to set the integer array Values divided by the number of students (average of commits)
        private void SetAverageValues()
        {
            // Initialize double array
            AverageValues = new double[Values.Length];

            // Sets the average integer values into the Values array
            for (int i = 0; i < Values.Length; i++)
            {
                // Calculate average value, set into double array
                AverageValues[i] = (double)Values[i] / (double)nrOfStudents;

                // Sets the values to '1' if the double value is between 0 and 1
                if (AverageValues[i] > 0 && AverageValues[i] < 1)
                {
                    Values[i] = 1;
                }

                // Converts the double array into the integer array
                if (Values[i] != 1)
                {
                    Values[i] = (int)AverageValues[i];
                }
            }
        }

        // Method which draws the graph with average commitinfo of all students onto a bitmap
        public override void Draw(Bitmap bitmap)
        {
            // Initialize Graphics and Rectangle[] fields
            g = Graphics.FromImage(bitmap);
            arrayGraphPerDay = new Rectangle[Values.Length];

            // Draw Recangle to use as border of the bitmap
            g.DrawRectangle(blackPen, 0, 0, bitmap.Width, bitmap.Height);
            BitmapSetValues(bitmap);


            // Draw vertical graph lines and commitblocks
            for (int j = 0; j < Values.Length; j++)
            {
                // Check if the commitvalues of a date are above the maximum limit - If yes, set maximum limit
                graphPerCommit = Values[j];
                if (graphPerCommit > CommitUpperLimit)
                {
                    graphPerCommit = CommitUpperLimit;
                }

                //Draw graphs of commits per day, fill with gray color
                arrayGraphPerDay[j] = new Rectangle(widthDivideByValues * j, 0, (int)(widthDivideByValues * 0.8), heightDivideByLimit * graphPerCommit);
                g.FillRectangle(new SolidBrush(Color.Gray), arrayGraphPerDay[j]);
            }
        }

        // Sets some important values that can be used inside the Draw method
        private void BitmapSetValues(Bitmap bitmap)
        {
            heightDivideByLimit = bitmap.Height / CommitUpperLimit;
            widthDivideByValues = bitmap.Width / Values.Length;
        }
    }
}
