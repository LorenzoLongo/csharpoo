﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LogicLayer;
using Globals.Interfaces;
using Globals.Classes;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace UnitTests
{
    [TestClass]
    public class TestCommitStatistics
    {
        [TestMethod, Timeout(100)]
        public void ImplementsInterface()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(CommitStatistics),
                                                 implementedInterface: typeof(IDrawable));
        }

        [TestMethod, Timeout(100)]
        public void HasRightConstructors()
        {
            Helper.CheckConstructors(typeof(CommitStatistics),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(AssignmentInfo), typeof(List<CommitInfo>) });
        }

        [TestMethod, Timeout(100)]
        public void HasRightProtectedFields()
        {
            Helper.CheckField(typeof(CommitStatistics), fieldName: "CommitUpperLimit",
                                                        fieldTypes: new Type[] { typeof(int) },
                                                        isStatic: true,
                                                        isProtected: true,
                                                        isConstant: true,
                                                        isReadOnly: false);
            Helper.CheckField(typeof(CommitStatistics), fieldName: "Values",
                                                        fieldTypes: new Type[] { typeof(int[]) },
                                                        isStatic: false,
                                                        isProtected: true,
                                                        isConstant: false,
                                                        isReadOnly: false);
        }

        [TestMethod, Timeout(1000)]
        public void CanInstantiate()
        {
            var x = new CommitStatistics(
                                            new AssignmentInfo("test", DateTime.Now.AddDays(-1), DateTime.Now),
                                            new List<CommitInfo>()
                                        );
            Assert.IsNotNull(x, $"Could not instantiate \' CommitStatistics\'");
        }

        [TestMethod, Timeout(1000)]
        public void InitialisesValuesOK()
        {
            var x = new CommitStatisticsProxy(
                                            new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                            new List<CommitInfo>()
                                            {
                                                new CommitInfo("A", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("B", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                            }
                                        );
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            Assert.IsNotNull(x.CommitValues, $"\'Values\' property is null after calling \'CommitStatistics\' constructor.");

        }

        [TestMethod, Timeout(1000)]
        public void CalculatesValuesOK()
        {
            var x = new CommitStatisticsProxy(
                                            new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                            new List<CommitInfo>()
                                            {
                                                new CommitInfo("A", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("B", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                            });
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            Assert.IsNotNull(x.CommitValues, $"\'Values\' property is null after calling \'CommitStatistics\' constructor.");
            Assert.IsTrue(x.CommitValues.Length == 2, $"\'CommitStatistics\' constructor test initializes \'Values\' property with length {x.CommitValues.Length}, should be 2.");
            Assert.IsTrue(x.CommitValues[0] == 0, $"\'CommitStatistics\' constructor test initializes \'Values\' property incorrect.");
            Assert.IsTrue(x.CommitValues[1] == 2, $"\'CommitStatistics\' constructor test initializes \'Values\' property incorrect.");
        }

        class CommitStatisticsProxy:CommitStatistics
        {
            public int[] CommitValues => Values;

            public CommitStatisticsProxy(AssignmentInfo assignment, List<CommitInfo> commits)
                : base(assignment, commits)
            { }
        }


        [TestMethod, Timeout(1000)]
        public void DrawBorderTest()
        {
            var x = new CommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() { });
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            const int size = 100;
            using(var bitmap = new Bitmap(size, size))
            using(var originalBitmap = new Bitmap(bitmap))
            {
                x.Draw(bitmap);
                Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width - 1), $"CommitStatistics \'Draw\' method does no drawing.");
                Assert.IsTrue(HasBorder(bitmap, originalBitmap), $"CommitStatistics \'Draw\' method does not draw a border.");
            }
        }

        [TestMethod, Timeout(1000)]
        public void DrawsCommits()
        {
            var x = new CommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() { });
            var y = new CommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() {
                                                new CommitInfo("1", DateTime.Now.AddDays(-1).AddMinutes(1)),
                                                new CommitInfo("2", DateTime.Now.AddDays(-1).AddMinutes(2)),
                                                new CommitInfo("3", DateTime.Now.AddDays(-1).AddMinutes(3)),
                                                new CommitInfo("4", DateTime.Now.AddDays(-1).AddMinutes(4)),
                                                new CommitInfo("5", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("6", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                                new CommitInfo("7", DateTime.Now.AddDays(-1).AddMinutes(7)),
                                                new CommitInfo("8", DateTime.Now.AddDays(-1).AddMinutes(8)),
                                                new CommitInfo("9", DateTime.Now.AddDays(-1).AddMinutes(9)),
                                                new CommitInfo("10", DateTime.Now.AddDays(-1).AddMinutes(10))
                                            });
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            Assert.IsNotNull(y, $"Could not instantiate \'CommitStatisticsProxy\' with filled commit-list.");
            const int size = 100;
            using(var bitmap = new Bitmap(size, size))
            {
                x.Draw(bitmap);
                using(var originalBitmap = new Bitmap(bitmap))
                {
                    y.Draw(bitmap);
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width - 1), $"CommitStatistics \'Draw\' method does not draw commit-info");
                }
            }
        }

        [TestMethod, ]
        public void DrawsCommitsPlausibleX()
        {
            var x = new CommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() { });
            var y = new CommitStatistics(new AssignmentInfo("test", DateTime.Now.AddDays(-2), DateTime.Now),
                                         new List<CommitInfo>() {
                                                new CommitInfo("1", DateTime.Now.AddDays(-1).AddMinutes(1)),
                                                new CommitInfo("2", DateTime.Now.AddDays(-1).AddMinutes(2)),
                                                new CommitInfo("3", DateTime.Now.AddDays(-1).AddMinutes(3)),
                                                new CommitInfo("4", DateTime.Now.AddDays(-1).AddMinutes(4)),
                                                new CommitInfo("5", DateTime.Now.AddDays(-1).AddMinutes(5)),
                                                new CommitInfo("6", DateTime.Now.AddDays(-1).AddMinutes(6)),
                                                new CommitInfo("7", DateTime.Now.AddDays(-1).AddMinutes(7)),
                                                new CommitInfo("8", DateTime.Now.AddDays(-1).AddMinutes(8)),
                                                new CommitInfo("9", DateTime.Now.AddDays(-1).AddMinutes(9)),
                                                new CommitInfo("10", DateTime.Now.AddDays(-1).AddMinutes(10))
                                            });
            Assert.IsNotNull(x, $"Could not instantiate \'CommitStatisticsProxy\'");
            Assert.IsNotNull(y, $"Could not instantiate \'CommitStatisticsProxy\' with filled commit-list.");
            const int size = 100;
            using(var bitmap = new Bitmap(size, size))
            {
                x.Draw(bitmap);
                using(var originalBitmap = new Bitmap(bitmap))
                {
                    y.Draw(bitmap);
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width - 1), $"CommitStatistics \'Draw\' method does not draw commit-info");
                    Assert.IsTrue(Helper.IsSameBitmap(bitmap, originalBitmap, 0, bitmap.Width/3), $"CommitStatistics \'Draw\' method does not draw commit-info correctly");
                    Assert.IsFalse(Helper.IsSameBitmap(bitmap, originalBitmap, (2*bitmap.Width) / 3, bitmap.Width - 1), $"CommitStatistics \'Draw\' method does not draw commit-info correctly");
                }
            }
        }
               
        private bool HasBorder(Bitmap b, Bitmap originalBitmap)
        {
            var borderColor = b.GetPixel(0, 0);
            if(originalBitmap.GetPixel(0, 0) == borderColor) return false;
            for(int i = 0; i < b.Width; i++)
            {
                if(b.GetPixel(i, 0) != borderColor) return false;
                if(b.GetPixel(i, b.Height - 1) != borderColor) return false;
            }            
            for(int i = 0; i < b.Height; i++)
            {
                if(b.GetPixel(0,i) != borderColor) return false;
                if(b.GetPixel(b.Width - 1,i) != borderColor) return false;
            }
            return true;
        }
    }
}
