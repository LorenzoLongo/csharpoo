﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommitMonitorMain;
using Globals.Interfaces;
using System.Reflection;
using System.Linq;
using LogicLayer;

namespace UnitTests
{
    [TestClass]
    public class TestFinalStructure
    {
        [TestMethod, Timeout(100)]
        public void MainFormLinksToLogicOK()
        {
            Helper.CheckConstructors(typeof(MainForm),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(ILogic) });
            Type x = typeof(MainForm);
            var fields = x.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            var OK = (fields.Where(f => f.FieldType == typeof(Logic)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'Logic\' (must use injected interface instead)!");            
        }
    }
       

}
