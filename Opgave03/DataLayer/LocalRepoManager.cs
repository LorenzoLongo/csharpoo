﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Globals.Interfaces;
using Globals.Classes;
using LibGit2Sharp;
using System.Configuration;

namespace DataLayer
{
    // Class which gets the number of commits inside a local repository
    public class LocalRepoManager : ILocalRepoManager
    {
        // Gets the current path of the repository
        public string CurrentRepoPath { get; private set; }

        // Constructor of the LocalRepoManager 
        public LocalRepoManager(string RepoPath)
        {
            SelectLocalRepo(RepoPath);
        }

        // Method which provides the selected repository path
        public void SelectLocalRepo(string path)
        {
            try
            {
                using (var repo = new Repository(path))
                {
                    CurrentRepoPath = path;
                }
            }
            catch (Exception)
            {
                CurrentRepoPath = string.Empty;
            }
        }

        // Method which gets the list of CommitInfo inside the assignment
        public List<CommitInfo> GetCommits(AssignmentInfo assignment)
        {
            var result = new List<CommitInfo>();
            if (CurrentRepoPath == string.Empty) return result;
            using (var repo = new Repository(CurrentRepoPath))
            {
                result = repo.Commits.Where(c => (c.Author.When.DateTime > assignment.Start) && (c.Author.When.DateTime < assignment.End))
                    .OrderBy(c => c.Author.When).Select(c => new CommitInfo(c.Id.ToString(), c.Author.When.DateTime)).ToList();
            }
            return result;
        }

    }
}
