﻿using Globals.Classes;
using Globals.Interfaces;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace CommitMonitorMain
{
    // Class which contains the events of the MainForm
    public partial class MainForm : Form
    {
        // Field to create a new FolderBrowserDialog
        private FolderBrowserDialog dialog;

        // Field to access logic layer
        private ILogic logic;

        // Field which is used to check the selected path
        private string checkPath;

        // Constructor of MainForm class
        public MainForm(ILogic logic)
        {
            this.logic = logic;
            InitializeComponent();
        }

        // Method to load the MainForm 
        private void MainForm_Load(object sender, EventArgs e)
        {
            // Textboxes are ReadOnly
            PathTxtBox.ReadOnly = true;
            DatesTxtBox.ReadOnly = true;
        }

        // Method which contains a clickevent for the SelectPathbtn
        private void SelectPathbtn_Click(object sender, EventArgs e)
        {
            // Create a new FolderBrowserDialog
            dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Create new path to check (if .gitattributes file isn't in directory, an exception is thrown)
                checkPath = dialog.SelectedPath.ToString() + "/.gitattributes";
                if (File.Exists(Path.Combine(Directory.GetCurrentDirectory(), checkPath)))
                {
                    // Send selected path to logical layer and add the current repositoryname to the TextBox
                    logic.SelectLocalRepo(dialog.SelectedPath);
                    PathTxtBox.Text = logic.CurrentRepoPath;

                    // Add assignments (by name) to the combobox
                    AssignmentComboBox.DataSource = logic.AssignmentInfos;
                    AssignmentComboBox.DisplayMember = "Name";
                }
                else
                {
                    MessageBox.Show("Geselecteerd pad is fout! Selecteer een GIT repoistory!", "Fout Pad", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Method which contains an event, if the selected assignment is changed 
        private void AssignmentComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Gets the index of the selected assignment inside the combobox
            var assignment = (AssignmentInfo)AssignmentComboBox.SelectedItem;
            logic.SelectAssignment(assignment);

            // Puts the start date and enddate of the selected assignment inside the TextBox
            DatesTxtBox.Text = assignment.Start.Date.ToShortDateString() + " ... " + assignment.End.Date.ToShortDateString();

            // Creates a new Bitmap inside the PictureBox and calls the UpdateUi method
            imagePicBox.Image = new Bitmap(1600, 1000);
            UpdateUi();
        }

        // Method which draws the chart on the bitmap and returns the bitmap to the PictureBox
        private void UpdateUi()
        {
            // Put image inside a new field, as Bitmap
            var bitmap = (Bitmap)imagePicBox.Image;

            // Draw Commitvalues onto the bitmap using the logic layer
            using (var g = Graphics.FromImage(bitmap)) g.Clear(Color.White);
            logic.PersonalCommits.Draw(bitmap);

            // Rotate the PictureBox and refresh it
            imagePicBox.Image.RotateFlip(RotateFlipType.Rotate180FlipX);
            this.imagePicBox.Refresh();
        }
    }
}
