﻿namespace CommitMonitorMain
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectPathbtn = new System.Windows.Forms.Button();
            this.PathTxtBox = new System.Windows.Forms.TextBox();
            this.AssignmentComboBox = new System.Windows.Forms.ComboBox();
            this.DatesTxtBox = new System.Windows.Forms.TextBox();
            this.imagePicBox = new System.Windows.Forms.PictureBox();
            this.LblAssignment = new System.Windows.Forms.Label();
            this.LblPath = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.imagePicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // SelectPathbtn
            // 
            this.SelectPathbtn.Location = new System.Drawing.Point(416, 330);
            this.SelectPathbtn.Name = "SelectPathbtn";
            this.SelectPathbtn.Size = new System.Drawing.Size(145, 23);
            this.SelectPathbtn.TabIndex = 0;
            this.SelectPathbtn.Text = "Select Path";
            this.SelectPathbtn.UseVisualStyleBackColor = true;
            this.SelectPathbtn.Click += new System.EventHandler(this.SelectPathbtn_Click);
            // 
            // PathTxtBox
            // 
            this.PathTxtBox.Location = new System.Drawing.Point(115, 304);
            this.PathTxtBox.Name = "PathTxtBox";
            this.PathTxtBox.Size = new System.Drawing.Size(446, 20);
            this.PathTxtBox.TabIndex = 1;
            // 
            // AssignmentComboBox
            // 
            this.AssignmentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AssignmentComboBox.FormattingEnabled = true;
            this.AssignmentComboBox.Location = new System.Drawing.Point(416, 266);
            this.AssignmentComboBox.Name = "AssignmentComboBox";
            this.AssignmentComboBox.Size = new System.Drawing.Size(145, 21);
            this.AssignmentComboBox.TabIndex = 2;
            this.AssignmentComboBox.SelectedIndexChanged += new System.EventHandler(this.AssignmentComboBox_SelectedIndexChanged);
            // 
            // DatesTxtBox
            // 
            this.DatesTxtBox.Location = new System.Drawing.Point(115, 267);
            this.DatesTxtBox.Name = "DatesTxtBox";
            this.DatesTxtBox.Size = new System.Drawing.Size(295, 20);
            this.DatesTxtBox.TabIndex = 3;
            // 
            // imagePicBox
            // 
            this.imagePicBox.Location = new System.Drawing.Point(115, 12);
            this.imagePicBox.Name = "imagePicBox";
            this.imagePicBox.Size = new System.Drawing.Size(446, 248);
            this.imagePicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imagePicBox.TabIndex = 4;
            this.imagePicBox.TabStop = false;
            // 
            // LblAssignment
            // 
            this.LblAssignment.AutoSize = true;
            this.LblAssignment.Location = new System.Drawing.Point(13, 274);
            this.LblAssignment.Name = "LblAssignment";
            this.LblAssignment.Size = new System.Drawing.Size(48, 13);
            this.LblAssignment.TabIndex = 5;
            this.LblAssignment.Text = "Opgave:";
            // 
            // LblPath
            // 
            this.LblPath.AutoSize = true;
            this.LblPath.Location = new System.Drawing.Point(13, 307);
            this.LblPath.Name = "LblPath";
            this.LblPath.Size = new System.Drawing.Size(57, 13);
            this.LblPath.TabIndex = 6;
            this.LblPath.Text = "Repo pad:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 365);
            this.Controls.Add(this.LblPath);
            this.Controls.Add(this.LblAssignment);
            this.Controls.Add(this.imagePicBox);
            this.Controls.Add(this.DatesTxtBox);
            this.Controls.Add(this.AssignmentComboBox);
            this.Controls.Add(this.PathTxtBox);
            this.Controls.Add(this.SelectPathbtn);
            this.Name = "MainForm";
            this.Text = "Commit Monitor";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imagePicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SelectPathbtn;
        private System.Windows.Forms.TextBox PathTxtBox;
        private System.Windows.Forms.ComboBox AssignmentComboBox;
        private System.Windows.Forms.TextBox DatesTxtBox;
        private System.Windows.Forms.PictureBox imagePicBox;
        private System.Windows.Forms.Label LblAssignment;
        private System.Windows.Forms.Label LblPath;
    }
}

