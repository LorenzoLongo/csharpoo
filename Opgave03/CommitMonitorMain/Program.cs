﻿using DataLayer;
using Globals.Interfaces;
using LogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CommitMonitorMain
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            ILocalRepoManager access = new LocalRepoManager(@"JSON\Assignments.json");
            IRemoteDataManager remote = new RemoteDataManager(@"JSON\Assignments.json");
            ILogic logic = new Logic(access, remote);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(logic));
        }
    }
}
