﻿using Globals.Classes;
using Globals.Interfaces;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace LogicLayer
{
    // Class which provides the information of every commit inside an assignment and draws it onto a bitmap
    public class CommitStatistics : IDrawable
    {
        // Maximum number of commits in a day
        protected const int CommitUpperLimit = 20;

        // Array which contains the number of commits per day
        protected int[] Values;

        // Date field
        private DateTime date;

        // Graphics class field
        private Graphics g;

        // Black pen field
        private Pen blackPen = new Pen(Color.Black, 3);

        // Constant fields
        private int heightDivideByLimit;
        private int widthDivideByValues;
        private int graphPerCommit;

        // Array of Rectangles
        private Rectangle[] arrayGraphPerDay;

        // Dictionary which contains the date and the number of commits that are done on that date
        private SortedDictionary<DateTime, int> Sorted = new SortedDictionary<DateTime, int>();

        // Constructor of CommitStatistics
        public CommitStatistics(AssignmentInfo assignment, List<CommitInfo> commits)
        {
            // Initialize Sorted list with dates and 0 integer value
            for (date = assignment.Start.Date; date < assignment.End.Date; date = date.AddDays(1))
            {
                Sorted.Add(date.Date, 0);
            }

            // Check on which day a commit of the CommitInfo List is done (increment integer value by 1)
            date = assignment.Start.Date;
            do
            {
                foreach (CommitInfo dates in commits)
                {
                    if (dates.TimeStamp.Date == date.Date)
                    {
                        Sorted[date.Date] += 1;
                    }
                }
                date = date.AddDays(1).Date;
            } while (date.Date < assignment.End.Date);

            // Initialize Values array and fill with the Sorted List integer values
            Values = new int[Sorted.Count];
            Values = Sorted.Values.ToArray();
        }

        // Method which draws the graph with commitinfo onto a bitmap
        public void Draw(Bitmap bitmap)
        {
            // Initialize Graphics and Rectangle[] fields
            g = Graphics.FromImage(bitmap);
            arrayGraphPerDay = new Rectangle[Values.Length];

            // Draw Recangle to use as border of the bitmap
            g.DrawRectangle(blackPen, 0, 0, bitmap.Width, bitmap.Height);
            BitmapSetValues(bitmap);

            // Draw horizontal graph lines
            for (int i = 0; i < CommitUpperLimit; i++)
            {
                g.DrawLine(blackPen, 0, heightDivideByLimit * i, bitmap.Width / 70, heightDivideByLimit * i);
            }

            // Draw vertical graph lines and commitblocks
            for (int j = 0; j < Values.Length; j++)
            {
                g.DrawLine(blackPen, widthDivideByValues * j, 0, widthDivideByValues * j, (bitmap.Height / 50));

                // Check if the commitvalues of a date are above the maximum limit - If yes, set maximum limit
                graphPerCommit = Values[j];
                if (graphPerCommit > CommitUpperLimit)
                {
                    graphPerCommit = CommitUpperLimit;
                }

                //Draw graphs of commits per day, fill with red color
                arrayGraphPerDay[j] = new Rectangle(widthDivideByValues * j, 0, widthDivideByValues / 2, heightDivideByLimit * graphPerCommit);
                g.FillRectangle(new SolidBrush(Color.Red), arrayGraphPerDay[j]);
            }
        }

        // Sets some important values that can be used inside the Draw method
        private void BitmapSetValues(Bitmap bitmap)
        {
            heightDivideByLimit = bitmap.Height / CommitUpperLimit;
            widthDivideByValues = bitmap.Width / Values.Length;
        }
    }
}
