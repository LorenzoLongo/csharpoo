﻿using Globals.Classes;
using Globals.Interfaces;
using System.Collections.Generic;

namespace LogicLayer
{
    // Class which contains the logical layer of the program 
    public class Logic : ILogic
    {
        // Array which contains the path but splitted
        private string[] splitPath;

        // Auto-Implemented property which gets a list of info from all assignments
        public List<AssignmentInfo> AssignmentInfos
        {
            get
            {
                return Remote.GetAssignments();
            }
        }

        // Auto-Implemented property which gets the name of the current repository from a given path
        public string CurrentRepoPath
        {
            get
            {
                // Split the path in a string array and return the name of the repository
                splitPath = Repo.CurrentRepoPath.Split('\\');
                return splitPath[splitPath.Length - 1];
            }
        }

        // Auto-Implemented property which draws the commits onto a bitmap
        public IDrawable PersonalCommits { get; private set; }

        // Readonly LocalRepoManager and RemoteDataManger interfaces fields
        private readonly ILocalRepoManager Repo;
        private readonly IRemoteDataManager Remote;

        // Constructor of the Logic class
        public Logic(ILocalRepoManager repo, IRemoteDataManager remote)
        {
            this.Repo = repo;
            this.Remote = remote;
        }

        // Method which provides the CommitInfo for a specific assignment, that needs to be drawn onto a bitmap
        public void SelectAssignment(AssignmentInfo assignment)
        {
            PersonalCommits = new CommitStatistics(assignment, Repo.GetCommits(assignment));
        }

        // Calls the SelectLocalRepo method of the ILocalRepoManager
        public void SelectLocalRepo(string path)
        {
            Repo.SelectLocalRepo(path);
        }
    }
}
