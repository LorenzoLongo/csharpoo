﻿using Globals.Classes;
using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface which contains the methods and properties for the logical layer of the program
    public interface ILogic
    {
        List<AssignmentInfo> AssignmentInfos { get; }

        string CurrentRepoPath { get; }

        IDrawable PersonalCommits { get; }

        void SelectAssignment(AssignmentInfo assignment);

        void SelectLocalRepo(string path);
    }
}
