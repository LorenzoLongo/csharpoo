﻿using Globals.Classes;
using System.Collections.Generic;

namespace Globals.Interfaces
{
    // Interface which contains methods to get the commit and assignmentinfo of all students (remotly)
    public interface IRemoteDataManager
    {
        List<CommitInfo> GetAllCommits(AssignmentInfo assignment);

        List<AssignmentInfo> GetAssignments();

        int GetNrOfStudents();
    }
}
