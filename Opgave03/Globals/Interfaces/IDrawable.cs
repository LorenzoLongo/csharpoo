﻿using System.Drawing;

namespace Globals.Interfaces
{
    // Interface which contains the Draw method (which draws the bitmap onto the PictureBox with all the CommitInfo)
    public interface IDrawable
    {
        void Draw(Bitmap bitmap);
    }
}
