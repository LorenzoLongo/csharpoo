﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave02
{
    public class TreeInfo
    {
        //initialize new List for Dictionary<string, List<FileInfo>>
        private List<FileInfo> ExtensionsList = new List<FileInfo>();

        //total amount of extensions
        private int Total;

        //auto-implement property depth directory
        public int Depth { get; private set; }

        //auto-implement property key(string): extension List<FileInfo> of files
        public Dictionary<string, List<FileInfo>> Files { get; private set; }

        //auto-implement property total number of directories
        public int NrOfDirectories { get; private set; }

        //auto-implement property total number of extensions
        public int NrOfExtensions { get { return Total; } }

        //auto-implement property total number of files
        public int NrOfFiles { get; private set; }


        //Method to get the top five of extensions inside the given path
        public List<ExtensionItem> GetTopFiveExtensions()
        {
            //initialize new List<ExtensionItem>
            List<ExtensionItem> extensionIt = new List<ExtensionItem>();

            //Insert Dictionary Files values inside enxtensionIt List
            foreach (var item in Files.Keys)
            {
                extensionIt.Add(new ExtensionItem(Files.Keys.ToString(), Files.Count));
            }

            //Sort List iwth CompareTo method in ExtensionItem class -> Reverse sort
            extensionIt.Sort();
            extensionIt.Reverse();

            //Check if the numver of extensions are less then 5 (if 'yes' use current List // if 'no' create new List with Top 5 extensions)
            if (extensionIt.Count <= 5)
            {
                return extensionIt;
            }
            else
            {
                //initialize new List
                List<ExtensionItem> listOrderedTopFive = new List<ExtensionItem>();

                //Fill List with top 5 extensions
                for (int i = 0; i < 5; i++)
                {
                    listOrderedTopFive.Add(extensionIt[i]);
                }

                return listOrderedTopFive;
            }
        }


        //Constructor TreeInfo: parameter given path
        public TreeInfo(string path)
        {
            //Check if path exists otherwise throw exception
            if (Directory.Exists(path))
            {
                //Starting Depth
                Depth = 1;
                NrOfDirectories = 0;
                NrOfFiles = 0;
                Files = new Dictionary<string, List<FileInfo>>();
                PopulateFiles(path, Depth);
            }
            else
            {
                throw new ArgumentException("Opgegeven pad bestaat niet!");
            }
        }


        //Method to count number of Directories/Files/Extensions and fill the Dictionary List with FileInfo
        private void PopulateFiles(string path, int depth)
        {
            //Empty catch-block for directories/files who don't have the right permissions
            try
            {
                //set Depth
                this.Depth = depth;

                //Add fileinfo to list
                foreach (string file in Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly))
                {
                    var info = new FileInfo(file);
                    if (!Files.ContainsKey(info.Extension))
                    {
                        //Fill List ExtensionsList // Fill Dictionary Files
                        ExtensionsList.Add(new FileInfo(info.Name));
                        Files.Add(info.Extension, ExtensionsList);

                        //Count total amount of extensions
                        Total = info.Extension.Count() + 1;
                    }
                }

                //recursion to get the maximum depth
                foreach (string subDir in Directory.GetDirectories(path))
                {
                    PopulateFiles(subDir, depth + 1);
                }

                //Get Nr of Directories
                String[] directories = Directory.GetDirectories(path);
                NrOfDirectories += directories.Length;

                //Get Nr of Files
                NrOfFiles += Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly).Length;
            }
            catch (Exception)
            {

            }
        }
    }
}
