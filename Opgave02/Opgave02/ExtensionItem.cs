﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave02
{
    public class ExtensionItem : IComparable<ExtensionItem>
    {
        //auto-implemented property counts the total amount an extension is found inside the given path
        public int Count { get; private set; }

        //auto-implemented property gets the extension value
        public string Extension { get; private set; }

        //Constructor ExtensionItem
        public ExtensionItem(string extension, int count)
        {
            this.Extension = extension;
            this.Count = count;
        }

        //CompareTo method to sort by counted amount an extension is found inside the given path
        public int CompareTo(ExtensionItem other)
        {
            return this.Count.CompareTo(other.Count);
        }
    }
}
