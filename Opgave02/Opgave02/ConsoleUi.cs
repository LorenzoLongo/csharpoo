﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave02
{
    public class ConsoleUi
    {
        //Gettermethod to get the path of the user input
        public string GetPath()
        {
            return Console.ReadLine();
        }

        //Method which provides information about the given path (Nr of directories, extensions, files and top 5 extensions + amount)
        public void RunTreeInfo(string path)
        {
            var info = new TreeInfo(path);
            Console.WriteLine();
            Console.WriteLine("Aantal mappen-niveau's: " + info.Depth + "\nAantal mappen: " + info.NrOfDirectories + "\nAantal bestanden: " + info.NrOfFiles + "\nAantal extensies: " + info.NrOfExtensions);
            Console.WriteLine("Top extensies:\n");
            foreach (var extensions in info.GetTopFiveExtensions())
            {
                Console.WriteLine(extensions.Extension + "\t:\t " + extensions.Count);
            }
            Console.WriteLine();
        }

        //Method which provides the information to end the Console application
        public void ShowEndBanner()
        {
            Console.WriteLine("<Enter> to continue");
            Console.ReadLine();
        }

        //Method which provides information to start the Console application and asks for user input (insert a 'path')
        public void ShowStartBanner()
        {
            Console.WriteLine("TreeInfo\n\nGeef een directory pad in: ");
        }
    }
}
