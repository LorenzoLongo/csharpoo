﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Opgave02
{
    public class Program
    {
        //Main program
        static void Main(string[] args)
        {
            //ConsoleUi object
            ConsoleUi tree = new ConsoleUi();

            //check if there is a command line argument (if no: ask user input)
            if (args.Length == 0)
            {
                tree.ShowStartBanner();
                tree.RunTreeInfo(tree.GetPath());
                tree.ShowEndBanner();
            }
            else if (args[0].Length != 0)
            {
                tree.ShowStartBanner();
                tree.RunTreeInfo(args[0]);
                tree.ShowEndBanner();
            }
        }
    }
}
