﻿namespace Opgave01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBoxOriginal = new System.Windows.Forms.PictureBox();
            this.picBoxProcessed = new System.Windows.Forms.PictureBox();
            this.btnReadImage = new System.Windows.Forms.Button();
            this.chkGreyscale = new System.Windows.Forms.CheckBox();
            this.chkGrid = new System.Windows.Forms.CheckBox();
            this.lblOriginal = new System.Windows.Forms.Label();
            this.lblProcessed = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxProcessed)).BeginInit();
            this.SuspendLayout();
            // 
            // picBoxOriginal
            // 
            this.picBoxOriginal.Location = new System.Drawing.Point(12, 45);
            this.picBoxOriginal.Name = "picBoxOriginal";
            this.picBoxOriginal.Size = new System.Drawing.Size(294, 208);
            this.picBoxOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxOriginal.TabIndex = 0;
            this.picBoxOriginal.TabStop = false;
            // 
            // picBoxProcessed
            // 
            this.picBoxProcessed.Location = new System.Drawing.Point(331, 45);
            this.picBoxProcessed.Name = "picBoxProcessed";
            this.picBoxProcessed.Size = new System.Drawing.Size(294, 208);
            this.picBoxProcessed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxProcessed.TabIndex = 1;
            this.picBoxProcessed.TabStop = false;
            // 
            // btnReadImage
            // 
            this.btnReadImage.Location = new System.Drawing.Point(12, 283);
            this.btnReadImage.Name = "btnReadImage";
            this.btnReadImage.Size = new System.Drawing.Size(96, 23);
            this.btnReadImage.TabIndex = 2;
            this.btnReadImage.Text = "Inlezen";
            this.btnReadImage.UseVisualStyleBackColor = true;
            this.btnReadImage.Click += new System.EventHandler(this.btnReadImage_Click);
            // 
            // chkGreyscale
            // 
            this.chkGreyscale.AutoSize = true;
            this.chkGreyscale.Location = new System.Drawing.Point(114, 270);
            this.chkGreyscale.Name = "chkGreyscale";
            this.chkGreyscale.Size = new System.Drawing.Size(87, 17);
            this.chkGreyscale.TabIndex = 3;
            this.chkGreyscale.Text = "Grijswaarden";
            this.chkGreyscale.UseVisualStyleBackColor = true;
            this.chkGreyscale.CheckedChanged += new System.EventHandler(this.chkGreyscale_CheckedChanged);
            // 
            // chkGrid
            // 
            this.chkGrid.AutoSize = true;
            this.chkGrid.Location = new System.Drawing.Point(114, 293);
            this.chkGrid.Name = "chkGrid";
            this.chkGrid.Size = new System.Drawing.Size(57, 17);
            this.chkGrid.TabIndex = 4;
            this.chkGrid.Text = "Raster";
            this.chkGrid.UseVisualStyleBackColor = true;
            // 
            // lblOriginal
            // 
            this.lblOriginal.AutoSize = true;
            this.lblOriginal.Location = new System.Drawing.Point(12, 9);
            this.lblOriginal.Name = "lblOriginal";
            this.lblOriginal.Size = new System.Drawing.Size(51, 13);
            this.lblOriginal.TabIndex = 5;
            this.lblOriginal.Text = "Origineel:";
            // 
            // lblProcessed
            // 
            this.lblProcessed.AutoSize = true;
            this.lblProcessed.Location = new System.Drawing.Point(328, 9);
            this.lblProcessed.Name = "lblProcessed";
            this.lblProcessed.Size = new System.Drawing.Size(52, 13);
            this.lblProcessed.TabIndex = 6;
            this.lblProcessed.Text = "Verwerkt:";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(328, 283);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(101, 13);
            this.lblStatus.TabIndex = 7;
            this.lblStatus.Text = "Geen foto gekozen!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 346);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblProcessed);
            this.Controls.Add(this.lblOriginal);
            this.Controls.Add(this.chkGrid);
            this.Controls.Add(this.chkGreyscale);
            this.Controls.Add(this.btnReadImage);
            this.Controls.Add(this.picBoxProcessed);
            this.Controls.Add(this.picBoxOriginal);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxProcessed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxOriginal;
        private System.Windows.Forms.PictureBox picBoxProcessed;
        private System.Windows.Forms.Button btnReadImage;
        private System.Windows.Forms.CheckBox chkGreyscale;
        private System.Windows.Forms.CheckBox chkGrid;
        private System.Windows.Forms.Label lblOriginal;
        private System.Windows.Forms.Label lblProcessed;
        private System.Windows.Forms.Label lblStatus;
    }
}

