﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Opgave01
{
    public partial class Form1 : Form
    {
        //private variables
        private Bitmap newImage, imageCopy;
        private int r, g, b;
        private Color pixelGrid;

        public Form1()
        {
            InitializeComponent();
        }

        //Add picture and filter on JPEG format
        private void btnReadImage_Click(object sender, EventArgs e)
        {
            OpenFileDialog readImage = new OpenFileDialog();
            readImage.Title = "Selecteer een foto";
            readImage.Filter = "JPEG only|*.jpg";

            if (readImage.ShowDialog() == DialogResult.OK)
            {
                //Copy JPEG  and copy Bitmap
                newImage = new Bitmap(readImage.FileName);
                imageCopy = new Bitmap(newImage);

                //Set bitmaps in pictureboxes
                picBoxOriginal.Image = newImage;
                picBoxProcessed.Image = imageCopy;
            }

            //Check status picture
            lblStatus.Text = "Foto toevoegen geslaagd!";
            lblStatus.Refresh();
        }

        //Draws grid onto the picture
        private void chkGrid_CheckedChanged(object sender, EventArgs e, PaintEventArgs d)
        {
            if(chkGreyscale.Checked && chkGrid.Checked)
            {
                greyscale();
                drawGrid();
            }
            else if(!chkGreyscale.Checked && chkGrid.Checked)
            {
                drawGrid();
            }

        }

        //Method which draws the grid
        private void drawGrid()
        {
            //Vertically draw lines
            for(int i = 0; i < imageCopy.Width; i++)
            {
                for(int j = 0; j < imageCopy.Height; j += 50)
                {
                    pixelGrid = imageCopy.GetPixel(i, j);
                    calculateRGB();
                    imageCopy.SetPixel(i, j, Color.FromArgb(r, g, b));
                }
            }

            //Horizontally draw lines
            for(int k = 0; k < imageCopy.Width; k += 50)
            {
                for(int l = 0; l < imageCopy.Height; l++)
                {
                    pixelGrid = imageCopy.GetPixel(k, l);
                    calculateRGB();
                    imageCopy.SetPixel(k, l, Color.FromArgb(r, g, b));
                }
            }

            //Draws picture with grid
            picBoxProcessed.Image = imageCopy;
        }


        //Calcultes the RGB values with XOR
        private void calculateRGB()
        {
            r = (byte)~(pixelGrid.R ^ 0);
            g = (byte)~(pixelGrid.G ^ 0);
            b = (byte)~(pixelGrid.B ^ 0);
        }


        //Method to greyscale the selected picture
        private void greyscale()
        {
            //Status greyscaling
            lblStatus.Text = "Aan het rekenen...";
            lblStatus.Refresh();

            //Calculates greyscale RGB values
            for (int i = 0; i < newImage.Width; i++)
            {
                for (int j = 0; j < newImage.Height; j++)
                {
                    Color pixel = imageCopy.GetPixel(i, j);
                    int grayscale = (pixel.R + pixel.G + pixel.B) / 3;
                    imageCopy.SetPixel(i, j, Color.FromArgb(grayscale, grayscale, grayscale));
                }
            }

            //Check status greyscaling
            picBoxProcessed.Image = imageCopy;
            lblStatus.Text = "Omzetting geslaagd!";
            lblStatus.Refresh();
        }


        //Greyscales the selected picture when checkbox is checked
        private void chkGreyscale_CheckedChanged(object sender, EventArgs e)
        {
            if(!chkGreyscale.Checked)
            {
                picBoxProcessed.Image = newImage;
            }
            else if(chkGreyscale.Checked)
            {
                //Status greyscaling
                lblStatus.Text ="Aan het rekenen...";
                lblStatus.Refresh();

                //Calculates greyscale RGB values
                for (int i=0; i < newImage.Width; i++)
                {
                    for(int j=0; j < newImage.Height; j++)
                    {
                        Color pixel = imageCopy.GetPixel(i, j);
                        int grayscale = (pixel.R + pixel.G + pixel.B) / 3;
                        imageCopy.SetPixel(i, j, Color.FromArgb(grayscale, grayscale, grayscale));
                    }
                }

                //Check status greyscaling
                picBoxProcessed.Image = imageCopy;
                lblStatus.Text = "Omzetting geslaagd!";
                lblStatus.Refresh();
            }

        }
    }
}
