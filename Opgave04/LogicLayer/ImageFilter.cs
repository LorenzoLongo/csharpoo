﻿using Globals;
using System;
using System.Drawing;

namespace LogicLayer
{
    public class ImageFilter : IFilter
    {
        // Auto-implemented Bitmap properties 
        public Bitmap OriginalImage { set; get; }
        public Bitmap FilteredImage { get; private set; }

        // Event which contains the value of the progress bar
        public event Action<int> PartProcessed;

        // Private fields
        private int heightcheck = 0, procentage, value = 0;

        // Method which loads the selected file into the Original bitmap and copies it into the Filtered bitmap
        public void Load(string file)
        {
            OriginalImage = (Bitmap)Image.FromFile(file);
            FilteredImage = new Bitmap(OriginalImage);
        }

        // Method which contains the delegate methods (each method [lambda expressions] sets a filter)
        public void ApplyFilter(Filter filterMode)
        {
            value = 0;

            // Switch case to select the selected filter from the combobox inside the form
            switch (filterMode)
            {
                case Filter.Original:
                    // To escape from an error if the form is loaded for the first time
                    if (OriginalImage == null)
                    {
                        break;
                    }
                    else
                    {
                        FilteredImage = new Bitmap(OriginalImage);
                    }
                    break;
                case Filter.Negative:
                    FilterOperation negative = (Color pixel) => pixel = Color.FromArgb((255 - pixel.R), (255 - pixel.G), (255 - pixel.B));
                    ExecuteFilter(negative);
                    break;
                case Filter.Red:
                    FilterOperation red = (Color pixel) => pixel = Color.FromArgb(pixel.R, 0, 0);
                    ExecuteFilter(red);
                    break;
                case Filter.Green:
                    FilterOperation green = (Color pixel) => pixel = Color.FromArgb(0, pixel.G, 0);
                    ExecuteFilter(green);
                    break;
                case Filter.Blue:
                    FilterOperation blue = (Color pixel) => pixel = Color.FromArgb(0, 0, pixel.B);
                    ExecuteFilter(blue);
                    break;
            }
        }

        // Method which gets the method for filtering and then applies it to each pixel of the original picture
        public void ExecuteFilter(FilterOperation operation)
        {
            // Check if a picture is selected
            if (FilteredImage == null)
            {
                throw new ArgumentNullException("Er is geen afbeelding geselecteerd");
            }
            else
            {
                // Sets the number of of height lines is equal to 1%
                procentage = (int)Math.Floor((double)(OriginalImage.Height / 100));

                // Run through every pixel and set new value
                for (int i = 0; i < OriginalImage.Width; i++)
                {
                    for (int j = 0; j < OriginalImage.Height; j++)
                    {
                        // If the heightcheck field is equal to the value of 1%, reset heightcheck value and increment PartProcessed event by 1
                        heightcheck++;
                        if (heightcheck == procentage)
                        {
                            heightcheck = 0;
                            if (PartProcessed != null && value <= 100) PartProcessed(value++);
                        }

                        // Get the pixel, do the delegate operation and set the new pixel value
                        Color pixel = OriginalImage.GetPixel(i, j);
                        pixel = operation(pixel);
                        FilteredImage.SetPixel(i, j, Color.FromArgb(pixel.R, pixel.G, pixel.B));

                    }
                }
            }
        }
    }
}
