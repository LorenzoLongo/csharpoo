﻿using System;
using System.Drawing;

namespace Globals
{
    // Interface which contains all the methods, properties of the logic layer
    public interface IFilter
    {

        Bitmap OriginalImage { get; set; }
        Bitmap FilteredImage { get; }

        event Action<int> PartProcessed;

        void Load(string file);
        void ApplyFilter(Filter filterMode);
    }
}
