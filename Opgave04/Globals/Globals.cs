﻿using System.Drawing;

namespace Globals
{
    // Enum which contains all of the filter values (for the combobox)
    public enum Filter { Original, Negative, Red, Green, Blue }

    // Delegation which contains the multiple filtermethods
    public delegate Color FilterOperation(Color pixel);
}
