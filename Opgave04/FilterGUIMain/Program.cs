﻿using Globals;
using LogicLayer;
using System;
using System.Windows.Forms;

namespace FilterGUIMain
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            // Linking logic layer with the form
            IFilter logic = new ImageFilter();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm(logic));
        }
    }
}
