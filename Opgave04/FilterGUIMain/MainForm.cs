﻿using Globals;
using System;
using System.IO;
using System.Windows.Forms;

namespace FilterGUIMain
{
    public partial class MainForm : Form
    {
        // Private fields 
        private IFilter logic;
        private OpenFileDialog dialog;
        private Filter selectedFilter;

        // Constructor of the MainForm
        public MainForm(IFilter logic)
        {
            // Initializes logic field, sets all enum values into the combobox
            this.logic = logic;
            InitializeComponent();

            filtersComboBox.DataSource = Enum.GetValues(typeof(Filter));
            filtersComboBox.SelectedIndex = 0;

            // Connects the event from the Logic class with an EventHandler
            logic.PartProcessed += ProgressChangedEventHandler;
        }

        // Method to load the form
        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        // Method which opens a FileDialog to select an image
        private void loadImageBtn_Click(object sender, EventArgs e)
        {
            // Initializes OpenFileDialog, sets title, sets starting directory
            dialog = new OpenFileDialog();
            dialog.Title = "Select image";
            dialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\resources";

            // Opens a FileDialog, loads the selected file into the logic class and sets the original image into the picturebox
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                logic.Load(dialog.FileName);
                imagePicBox.Image = logic.FilteredImage;
            }
        }

        // Method which contains all the filters inside the combobox and implements methods of the logic layer
        private void filtersComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Shows an error if there is nog picture selected
            try
            {
                // Gets the selected filter value (from the combobox) and sets the ApplyFilter method from the logic layer
                selectedFilter = (Filter)filtersComboBox.SelectedItem;
                logic.ApplyFilter(selectedFilter);

                // Updates the picturebox with the filtered picture
                imagePicBox.Image = logic.FilteredImage;
                imagePicBox.Refresh();
            }
            catch (ArgumentNullException err)
            {
                MessageBox.Show(err.Message, "NO PHOTO SELECTED", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        // Eventhandler which updates the progress of the filter editing inside the progress bar
        public void ProgressChangedEventHandler(int count)
        {
            filterProgressBar.Value = count;
            filterProgressBar.Refresh();
        }
    }
}
