﻿namespace FilterGUIMain
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imagePicBox = new System.Windows.Forms.PictureBox();
            this.loadImageBtn = new System.Windows.Forms.Button();
            this.filtersComboBox = new System.Windows.Forms.ComboBox();
            this.filterProgressBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.imagePicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // imagePicBox
            // 
            this.imagePicBox.Location = new System.Drawing.Point(12, 12);
            this.imagePicBox.Name = "imagePicBox";
            this.imagePicBox.Size = new System.Drawing.Size(330, 198);
            this.imagePicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imagePicBox.TabIndex = 0;
            this.imagePicBox.TabStop = false;
            // 
            // loadImageBtn
            // 
            this.loadImageBtn.Location = new System.Drawing.Point(376, 12);
            this.loadImageBtn.Name = "loadImageBtn";
            this.loadImageBtn.Size = new System.Drawing.Size(121, 42);
            this.loadImageBtn.TabIndex = 1;
            this.loadImageBtn.Text = "Load Image";
            this.loadImageBtn.UseVisualStyleBackColor = true;
            this.loadImageBtn.Click += new System.EventHandler(this.loadImageBtn_Click);
            // 
            // filtersComboBox
            // 
            this.filtersComboBox.FormattingEnabled = true;
            this.filtersComboBox.Location = new System.Drawing.Point(376, 80);
            this.filtersComboBox.Name = "filtersComboBox";
            this.filtersComboBox.Size = new System.Drawing.Size(121, 21);
            this.filtersComboBox.TabIndex = 2;
            this.filtersComboBox.UseWaitCursor = true;
            this.filtersComboBox.SelectedIndexChanged += new System.EventHandler(this.filtersComboBox_SelectedIndexChanged);
            // 
            // filterProgressBar
            // 
            this.filterProgressBar.Location = new System.Drawing.Point(12, 222);
            this.filterProgressBar.Name = "filterProgressBar";
            this.filterProgressBar.Size = new System.Drawing.Size(330, 23);
            this.filterProgressBar.Step = 1;
            this.filterProgressBar.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 257);
            this.Controls.Add(this.filterProgressBar);
            this.Controls.Add(this.filtersComboBox);
            this.Controls.Add(this.loadImageBtn);
            this.Controls.Add(this.imagePicBox);
            this.Name = "MainForm";
            this.Text = "Filter Gui - Opgave 04";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.imagePicBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imagePicBox;
        private System.Windows.Forms.Button loadImageBtn;
        private System.Windows.Forms.ComboBox filtersComboBox;
        private System.Windows.Forms.ProgressBar filterProgressBar;
    }
}

