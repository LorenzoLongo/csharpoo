﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using System.Linq;
using System.Drawing;
using System.IO;
using Globals;
using LogicLayer;
using FilterGUIMain;

namespace UnitTests
{
    static class Helper
    {
        static private bool dummy = LoadAllAssemblies();
        private static bool LoadAllAssemblies()
        {

            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var di = new DirectoryInfo(path);
            var debug = di.GetFiles("*.*");
            foreach(var file in di.GetFiles("*.dll"))
            {
                try
                {
                    var nextAssembly = Assembly.LoadFile(file.FullName);
                }
                catch(BadImageFormatException)
                {
                    // Not a .net assembly  - ignore
                }
            }
            return true;
        }

        public static Type GetTypeByName(string typeName)
        {
            var x = AppDomain.CurrentDomain.GetAssemblies();
            var foundClass = (from assembly in AppDomain.CurrentDomain.GetAssemblies()
                              from type in assembly.GetTypes()
                              where type.Name == typeName
                              select type).FirstOrDefault();
            return (Type)foundClass;
        }
        public static void CheckProperty(Type t, string propName, Type[] propTypes, bool hasPublicGetter, bool hasPublicSetter, bool hasPrivateGetter, bool hasPrivateSetter)
        {
            var props = t.GetProperties();
            var prop = props.Where(p => p.Name == propName).FirstOrDefault();
            Assert.IsNotNull(prop, $"Type {t.GetType().Name} has no public \'{propName}\' property");
            Assert.IsTrue(Array.Exists(propTypes, p => p.Name == prop.PropertyType.Name),
                              $"{typeof(object).Name}: property \'{propName}\' is a {prop.PropertyType.Name}");
            if(hasPrivateGetter || hasPublicGetter)
            {
                Assert.IsNotNull(prop.GetMethod, $"{t.Name}: property {propName} has no Getter ");
                Assert.IsTrue((prop.GetMethod.IsPublic == hasPublicGetter), $"{t.Name}: property {propName} has {(prop.GetMethod.IsPublic ? "a" : "no")} public Getter ");
                Assert.IsTrue((prop.GetMethod.IsPrivate == hasPrivateGetter), $"{t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Getter ");
            }
            else
            {
                Assert.IsNull(prop.GetMethod, $"{t.Name}: property {propName} has a Getter (not allowed)");
            }
            if(hasPrivateSetter || hasPublicSetter)
            {
                Assert.IsNotNull(prop.SetMethod, $"{t.Name}: property {propName} has no Setter ");
                Assert.IsTrue((prop.SetMethod.IsPublic == hasPublicSetter), $"GlobalTests - {t.Name}: property {propName} has {(prop.SetMethod.IsPublic ? "a" : "no")} public Setter ");
                Assert.IsTrue((prop.SetMethod.IsPrivate == hasPrivateSetter), $"GlobalTests - {t.Name}: property {propName} has {(prop.GetMethod.IsPrivate ? "a" : "no")} private Setter ");
            }
            else
            {
                Assert.IsNull(prop.SetMethod, $"{t.Name}: property {propName} has a Setter (not allowed)");
            }
        }
        public static void CheckMethod(Type t, string methodName, Type[] returnTypes, Type[] parameterTypes)
        {
            var methods = t.GetMethods();
            // check if method exists with right signature
            var method = methods.Where(m =>
            {
                if(m.Name != methodName) return false;
                var parameters = m.GetParameters();
                if((parameterTypes == null || parameterTypes.Length == 0)) return parameters.Length == 0;
                if(parameters.Length != parameterTypes.Length) return false;
                for(int i = 0; i < parameterTypes.Length; i++)
                {
                    // if (parameters[i].ParameterType != parameterTypes[i])
                    if(!parameters[i].ParameterType.IsAssignableFrom(parameterTypes[i]))
                        return false;
                }
                return true;
            }).FirstOrDefault();
            Assert.IsNotNull(method, $"Type {t.FullName} has no public \'{methodName}\' method with the right signature");

            // check returnType
            Assert.IsTrue(Array.Exists(returnTypes, r => r.Name == method.ReturnType.Name),
                              $"Type {typeof(object).Name}: method \'{methodName}\' returns a \'{method.ReturnType.Name}\'");
        }
        public static void CheckClassImplementsInterface(Type implementingClass, Type implementedInterface)
        {
            Assert.IsTrue(implementedInterface.IsAssignableFrom(implementingClass), $"\'{implementingClass.Name}\' does not implement interface \'{implementedInterface}\'.");
        }
        public static void CheckConstructors(Type x, bool hasDefault, bool hasSpecific, Type[] specificParameters)
        {
            var constructor = x.GetConstructor(new Type[] { });
            if(hasDefault) Assert.IsNotNull(constructor, $"\'{x.Name}\' has no default constructor (which is required)!");
            if(!hasDefault) Assert.IsNull(constructor, $"\'{x.Name}\' has a default constructor (not allowed)!");
            if(hasSpecific)
            {
                constructor = x.GetConstructor(specificParameters);
                string parameters = specificParameters.Select(p => $"\'{p.Name}\'").Aggregate((temp, element) => temp + "," + element);
                Assert.IsNotNull(constructor,
                    $"\'{x.Name}\' does not contain a constructor with following parameter(s) of type(s): {parameters}.");
            }
        }
        public static void CheckField(Type t, string fieldName, Type[] fieldTypes, bool isStatic, bool isProtected, bool isConstant, bool isReadOnly)
        {
            var field = t.GetField(fieldName, BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.IsNotNull(field, $"Type {t.Name} has no \'{fieldName}\' field");
            Assert.IsTrue(fieldTypes.Contains(field.FieldType), $"Field \'{fieldName}\' should be of type \'{fieldTypes[0]}\' but is of type \'{field.FieldType}\'.");
            Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Public), $"Field \'{fieldName}\' is public but should not be.");
            if(isConstant) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Static) && field.Attributes.HasFlag(FieldAttributes.Literal), $"Field \'{fieldName}\' should be a constant but is not.");
            if(!isConstant)
            {
                Assert.IsTrue(!field.Attributes.HasFlag(FieldAttributes.Static) || !field.Attributes.HasFlag(FieldAttributes.Literal), $"Field \'{fieldName}\' is a constant but should not be.");
                if(isStatic) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Static), $"Field \'{fieldName}\' should be static but is not.");
                if(!isStatic) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Static), $"Field \'{fieldName}\' is static but should not be.");
            }
            if(isProtected) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.Family), $"Field \'{fieldName}\' should be protected (not private) but is not.");
            if(!isProtected) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.Family), $"Field \'{fieldName}\' is protected, should be private.");
            if(isReadOnly) Assert.IsTrue(field.Attributes.HasFlag(FieldAttributes.InitOnly), $"Field \'{fieldName}\' should be readonly but is not.");
            if(!isReadOnly) Assert.IsFalse(field.Attributes.HasFlag(FieldAttributes.InitOnly), $"Field \'{fieldName}\' is readonly but should not be.");
        }
        public static bool IsSameBitmap(Bitmap b1, Bitmap b2, int startColumn, int endColumn)
        {
            for(int i = startColumn; i <= endColumn; i++)
            {
                for(int j = 0; j < b1.Height; j++)
                {
                    if(b1.GetPixel(i, j) != b2.GetPixel(i, j)) return false;
                }
            }
            return true;
        }


    }



    [TestClass]
    public class TestArchitectuur
    {


        [TestMethod, Timeout(100)]
        public void MainFormLinksToLogicOK()
        {
            Helper.CheckConstructors(typeof(MainForm),
                                     hasDefault: false,
                                     hasSpecific: true,
                                     specificParameters: new Type[] { typeof(IFilter) });
            Type x = typeof(MainForm);
            var fields = x.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            var OK = (fields.Where(f => f.FieldType == typeof(ImageFilter)).Count() == 0);
            Assert.IsTrue(OK, $"\'{x.Name}\' has a local field for \'Logic\' (must use injected interface instead)!");
        }

        [TestMethod, Timeout(100)]
        public void TestImageFilterImplementsIFilter()
        {
            Helper.CheckClassImplementsInterface(implementingClass: typeof(ImageFilter),
                                                implementedInterface: typeof(IFilter));
        }

        [TestMethod, Timeout(100)]
        public void TestIfIFilterContainsEvent()
        {
            var eventInfo = typeof(IFilter).GetEvents().FirstOrDefault();
            Assert.IsNotNull(eventInfo, $"\'IFilter\' interface does not declare any events.");
            if(eventInfo != null)
            {
                var x = eventInfo.EventHandlerType;
                var parameters = x.GetMethod("Invoke").GetParameters();
                Assert.IsTrue((parameters.Length == 1), $"\'IFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
                Assert.IsTrue((parameters[0].ParameterType.Name == "Int32"), $"\'IFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
            };
        }

    }

    [TestClass]

    public class TestImageFilter
    {
        static private Random random;

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            random = new Random();
        }
        
        public static Bitmap GetTestBitmap()
        {
            int w = random.Next(50) + 1;
            int h = random.Next(50) + 1;
            lock(random)
            {
                var bitmap = new Bitmap(w, h);
                for(int x = 0; x < bitmap.Width; x++)
                {
                    for(int y = 0; y < bitmap.Height; y++)
                    {
                        bitmap.SetPixel(x, y, Color.FromArgb(random.Next(255), random.Next(255), random.Next(255)));
                    }
                }
                return bitmap;
            }
        }

        private void SaveBitmap(Bitmap bitmap, string filename)
        {
            using(var bm = new Bitmap(bitmap))
            {
                bm.Save(filename, System.Drawing.Imaging.ImageFormat.Png);
            }
        }
        

        [TestMethod, Timeout(2000)]
        public void TestLoad()
        {
            var filename = "test.png";
            IFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();                
            }
        }

        private void TestLoadHelper(IFilter filter, Bitmap original)
        {
            var filteredBitmap = filter.FilteredImage;
            Assert.IsNotNull(filteredBitmap, $"ImageFilter - after call to \'Load\', property \'FilteredImage\' is null.");
            Assert.IsTrue(((original.Width == filteredBitmap.Width) && (original.Height == filteredBitmap.Height)),
                         $"ImageFilter - after call to \'Load\', \'filteredImage\' has wrong size.");
            for(int x = 0; x < filteredBitmap.Width; x++)
            {
                for(int y = 0; y < filteredBitmap.Height; y++)
                {
                    var t = original.GetPixel(x, y);
                    var c = filteredBitmap.GetPixel(x, y);
                    Assert.IsTrue(
                        ((c.R == t.R) &&
                         (c.G == t.G) &&
                         (c.B == t.B)), $"ImageFilter - after call to \'Load\', property \'FilteredImage\' contains wrong bitmap.");
                }
            }
        }

        private void AttachDummyEventHandler(ImageFilter filter)
        {
            EventInfo eventInfo = typeof(ImageFilter).GetEvents().FirstOrDefault();
            if(eventInfo != null)
            {
                var x = eventInfo.EventHandlerType;
                var parameters = x.GetMethod("Invoke").GetParameters();
                Assert.IsTrue((parameters.Length == 1), $"\'ImageFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
                Assert.IsTrue((parameters[0].ParameterType.Name == "Int32"), $"\'ImageFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
                MethodInfo addHandler = eventInfo.GetAddMethod();
                Action<int> dummy = (i) => { };
                Object[] addHandlerArgs = { dummy };
                addHandler.Invoke(filter, addHandlerArgs);
            };
        }
        
        [TestMethod, Timeout(2000)]
        public void TestExecuteFilter()
        {

            var filename = "test1.png";
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                AttachDummyEventHandler(filter);
                filter.ExecuteFilter((p) => { return Color.FromArgb(1, 2, 3); });
                var filteredBitmap = filter.FilteredImage;
                Assert.IsNotNull(filteredBitmap, $"ImageFilter - upon calling \'ExecuteFilter\', property \'FilteredImage\' is null.");
                var originalBitmap = filter.OriginalImage;
                Assert.IsTrue(((originalBitmap.Width == filteredBitmap.Width) && (originalBitmap.Height == filteredBitmap.Height)),
                             $"ImageFilter - upon calling \'ExecuteFilter\', property \'filteredImage\' has wrong size.");
                for(int x = 0; x < filteredBitmap.Width; x++)
                {
                    for(int y = 0; y < filteredBitmap.Height; y++)
                    {
                        var c = filteredBitmap.GetPixel(x, y);
                        Assert.IsTrue(
                            ((c.R == 1) &&
                             (c.G == 2) &&
                             (c.B == 3)), $"ImageFilter - upon calling \'ExecuteFilter\', property \'FilteredImage\' contains wrong bitmap.");
                    }
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }

        }
        
        [TestMethod, Timeout(2000)]
        public void TestApplyNegativeFilter()
        {
            var filename = "test2.png";
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                AttachDummyEventHandler(filter);


                filter.ApplyFilter(Filter.Negative);
                var filteredBitmap = filter.FilteredImage;
                Assert.IsNotNull(filteredBitmap, $"ImageFilter - upon calling \'ApplyFilter\', property \'FilteredImage\' is null.");
                var originalBitmap = filter.OriginalImage;
                Assert.IsTrue(((originalBitmap.Width == filteredBitmap.Width) && (originalBitmap.Height == filteredBitmap.Height)),
                             $"ImageFilter - upon calling \'ApplyFilter\', property \'FilteredImage\' has wrong size.");
                for(int x = 0; x < filteredBitmap.Width; x++)
                {
                    for(int y = 0; y < filteredBitmap.Height; y++)
                    {
                        var f = filteredBitmap.GetPixel(x, y);
                        var o = originalBitmap.GetPixel(x, y);
                        int c = f.ToArgb()|o.ToArgb() ;
                        Assert.IsTrue((c == -1),
                            $"ImageFilter - upon calling \'ApplyFilter(Filter.Negative)\', property \'FilteredImage\' contains wrong bitmap.");
                    }
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }

        [TestMethod, Timeout(2000)]
        public void TestApplyRedFilter()
        {
            var filename = "test3.png";
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                AttachDummyEventHandler(filter);
                filter.ApplyFilter(Filter.Red);
                var filteredBitmap = filter.FilteredImage;
                Assert.IsNotNull(filteredBitmap, $"ImageFilter - upon calling \'ApplyFilter\', property \'FilteredImage\' is null.");
                var originalBitmap = filter.OriginalImage;
                Assert.IsTrue(((originalBitmap.Width == filteredBitmap.Width) && (originalBitmap.Height == filteredBitmap.Height)),
                             $"ImageFilter - upon calling \'ApplyFilter\', property \'filteredImage\' has wrong size.");
                for(int x = 0; x < filteredBitmap.Width; x++)
                {
                    for(int y = 0; y < filteredBitmap.Height; y++)
                    {
                        var c = filteredBitmap.GetPixel(x, y);
                        Assert.IsTrue(
                            ((c.G == 0) && (c.B == 0)),
                            $"ImageFilter - upon calling \'ApplyFilter(Filter.Red)\', property \'FilteredImage\' contains wrong bitmap.");
                    }
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }

        [TestMethod, Timeout(2000)]
        public void TestApplyGreenFilter()
        {
            var filename = "test4.png";
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                AttachDummyEventHandler(filter);
                filter.ApplyFilter(Filter.Green);
                var filteredBitmap = filter.FilteredImage;
                Assert.IsNotNull(filteredBitmap, $"ImageFilter - upon calling \'ApplyFilter\', property \'FilteredImage\' is null.");
                var originalBitmap = filter.OriginalImage;
                Assert.IsTrue(((originalBitmap.Width == filteredBitmap.Width) && (originalBitmap.Height == filteredBitmap.Height)),
                             $"ImageFilter - upon calling \'ApplyFilter\', property \'filteredImage\' has wrong size.");
                for(int x = 0; x < filteredBitmap.Width; x++)
                {
                    for(int y = 0; y < filteredBitmap.Height; y++)
                    {
                        var c = filteredBitmap.GetPixel(x, y);
                        Assert.IsTrue(
                            ((c.R == 0) && (c.B == 0)),
                            $"ImageFilter - upon calling \'ApplyFilter(Filter.Green)\', property \'FilteredImage\' contains wrong bitmap.");
                    }
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }

        [TestMethod, Timeout(2000)]
        public void TestApplyBlueFilter()
        {
            var filename = "test5.png";
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                AttachDummyEventHandler(filter);
                filter.ApplyFilter(Filter.Blue);
                var filteredBitmap = filter.FilteredImage;
                Assert.IsNotNull(filteredBitmap, $"ImageFilter - upon calling \'ApplyFilter\', property \'FilteredImage\' is null.");
                var originalBitmap = filter.OriginalImage;
                Assert.IsTrue(((originalBitmap.Width == filteredBitmap.Width) && (originalBitmap.Height == filteredBitmap.Height)),
                             $"ImageFilter - upon calling \'ApplyFilter\', property \'filteredImage\' has wrong size.");
                for(int x = 0; x < filteredBitmap.Width; x++)
                {
                    for(int y = 0; y < filteredBitmap.Height; y++)
                    {
                        var c = filteredBitmap.GetPixel(x, y);
                        Assert.IsTrue(
                            ((c.R == 0) && (c.G == 0)),
                            $"ImageFilter - upon calling \'ApplyFilter(Filter.Blue)\', property \'FilteredImage\' contains wrong bitmap.");
                    }
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }
        

        private void TestIfImageFilterContainsEvent()
        {
            var eventInfo = typeof(ImageFilter).GetEvents().FirstOrDefault();
            Assert.IsNotNull(eventInfo, $"Class \'ImageFilter\' does not declare any events.");
            if(eventInfo != null)
            {
                var x = eventInfo.EventHandlerType;
                var parameters = x.GetMethod("Invoke").GetParameters();
                Assert.IsTrue((parameters.Length == 1), $"Class \'ImageFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
                Assert.IsTrue((parameters[0].ParameterType.Name == "Int32"), $"Class \'ImageFilter\' event \'{eventInfo.Name}\' is not of type '\'Action<int>\'.");
            };
        }

        [TestMethod, Timeout(1000)]
        public void TestIfExecuteFilterCallsEvent()
        {
            var filename = "test6.png";
            TestIfExecuteFilterCallsEventHelper(filename);
        }

        private void TestIfExecuteFilterCallsEventHelper(string filename)
        {
            TestIfImageFilterContainsEvent();
            ImageFilter filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);
            try
            {
                TestLoadHelper(filter, testBitmap);
                int eventCounter = 0;
                int eventValue = -1;

                EventInfo eventInfo = typeof(ImageFilter).GetEvents().FirstOrDefault();
                if(eventInfo != null)
                {
                    var parameters = eventInfo.EventHandlerType.GetMethod("Invoke").GetParameters();
                    MethodInfo addHandler = eventInfo.GetAddMethod();
                    Action<int> dummy = (i) =>
                    {
                        eventCounter++;
                        eventValue = i;
                    };
                    Object[] addHandlerArgs = { dummy };
                    addHandler.Invoke(filter, addHandlerArgs);
                };
                filter.ExecuteFilter((p) => { return p; });
                Assert.IsTrue((eventCounter > 0), $"Method \'ExecuteFilter\' does not call its \'{eventInfo.Name}\' event.");
                Assert.IsTrue((eventCounter > Math.Min(testBitmap.Height, testBitmap.Width) / 2),
                    $"Method \'ExecuteFilter\' calls its \'{eventInfo.Name}\' event only {eventCounter} times for bitmap with {testBitmap.Height} lines of {testBitmap.Width} pixels.");
                Assert.IsTrue((eventValue >= 90), $"ExecuteFilter - Last value from \'{eventInfo.Name}\' event is {eventValue}, expected 100.");
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }

        [TestMethod]
        public void TestIfExecuteFilterTestsNullEvent()
        {
            var filename = "test7a.png";
            TestIfExecuteFilterCallsEventHelper(filename);
            filename = "test7b.png";
            var filter = new ImageFilter();
            var testBitmap = GetTestBitmap();
            SaveBitmap(testBitmap, filename);
            filter.Load(filename);

            try
            {
                TestLoadHelper(filter, testBitmap);
                try
                {
                    filter.ExecuteFilter((p) => { return p; });
                }
                catch(Exception e)
                {
                    Assert.Fail($"Method \'ExecuteFilter\' calls event even if it is null: \'{e.GetType().ToString()}\' is thrown.");
                }
            }
            finally
            {
                filter.OriginalImage.Dispose();
                filter.FilteredImage.Dispose();
            }
        }

    }
}
